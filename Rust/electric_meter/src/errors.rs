use failure_derive;
// use failure;

#[derive(Debug, Fail)]
pub enum EMError {
    #[fail(display = "Unknown data type in Yaml: {}", _0)]
    UnknownDataTypeError(&'static str),
    #[fail(display = "Invalid ModBus address: {}", _0)]
    InvalidAddressError(&'static str),
    #[fail(display = "No magick word in file: {}", _0)]
    NoMagickInFile(&'static str),
    #[fail(display = "Invalid YAML file: {}", _0)]
    InvalidYamlFile(&'static str),
    #[fail(display = "Cannot find this model of electric meter: {}", _0)]
    CannotFindModel(&'static str),
    #[fail(display = "Unreadable model file error: {}", _0)]
    CannotReadModelFile(&'static str),
    #[fail(display = "Cannot open model file: {}", _0)]
    CannotOpenModelFile(&'static str),
    #[fail(display = "Incorrect # of words read: {}", _0)]
    IncorrectNumWordsRead(&'static str),
    #[fail(display = "Unreachable code reached: {}", _0)]
    Unreachable(&'static str),
    #[fail(display = "Modbus error: {}", _0)]
    ModbusError(&'static str),
}

