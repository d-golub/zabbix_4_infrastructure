use std::string::String;
use std::convert::From;
use std::collections::HashMap;
// misc numbers
use num::Float;
use num::integer::Integer;
use num::{FromPrimitive, ToPrimitive};

use failure;

// Data types for electric meter.

// 1 measure: single-phase, two-phase or triple-phase.  Or NoData at all
#[derive(Debug, Serialize)]
#[serde(untagged)]
pub enum OptionalData {
    NoData,
    F(f64),
    I(i64),
    F2(f64, f64),
    I2(i64, i64),
    F3(f64, f64, f64),
    I3(i64, i64, i64),
}

// -- from integers --
impl OptionalData {

    fn from_i<I> (src: I) -> OptionalData
        where I: Integer + FromPrimitive + ToPrimitive
    {
        match src.to_i64() {
            Some(v) => OptionalData::I(v),
            None => OptionalData::NoData,
        }
    }

    fn from_f<F> (src: F) -> OptionalData
        where F: Float + FromPrimitive + ToPrimitive
    {
        match src.to_f64() {
            Some(v) => OptionalData::F(v),
            None => OptionalData::NoData,
        }
    }
}

// -- from Results and Options --
impl<I> From<Option<I>> for OptionalData 
    where I: Integer + FromPrimitive + ToPrimitive
{
    fn from (src: Option<I>) -> OptionalData
        where I: Integer + FromPrimitive + ToPrimitive
    {
        match src {
            Some(v) => {
                match v.to_i64() {
                    Some(v) => OptionalData::I(v),
                    None => OptionalData::NoData,
                }
            }
            None => OptionalData::NoData
        }
    }
}

impl<I> From<Result<I, failure::Error>> for OptionalData 
    where I: Integer + FromPrimitive + ToPrimitive
{
    fn from (src: Result<I, failure::Error>) -> OptionalData
        where I: Integer + FromPrimitive + ToPrimitive
    {
        match src.map(|v| 
            match v.to_i64() {
                Some(v) => OptionalData::I(v),
                None => OptionalData::NoData,
            })   // end of 'match' condition
        {
            Ok(od) => od,
            Err(e) => {
                error!("OptionalData::From: error {} on input", e);
                OptionalData::NoData
            },
        }
    }
}

impl OptionalData {
    pub fn from_opt_float<F> (src: Option<F>) -> OptionalData
        where F: Float + FromPrimitive + ToPrimitive
    {
        match src {
            Some(v) => {
                match v.to_f64() {
                    Some(v) => OptionalData::F(v),
                    None => OptionalData::NoData,
                }
            }
            None => OptionalData::NoData
        }
    }

    pub fn from_res_float<F> (src: Result<F, failure::Error>) -> OptionalData
        where F: Float + FromPrimitive + ToPrimitive
    {
        match src.map(|v|
            match v.to_f64() {
                Some(v) => OptionalData::F(v),
                None => OptionalData::NoData,
            })
        {
            Ok(od) => od,
            Err(_) => OptionalData::NoData,
        }
    }
}


// -- from slices --
impl<'a> From<&'a [i16]> for OptionalData {
    fn from (src: &[i16]) -> OptionalData {
        match src.len() {
            0 => {
                OptionalData::NoData
            },
            1 => {
                OptionalData::I(src[0] as i64)
            },
            2 => {
                OptionalData::I2(src[0] as i64, src[1] as i64)
            },
            3 => {
                OptionalData::I3(src[0] as i64, src[1] as i64, src[2] as i64)
            },
            _ => {  // four or more
                error!("Four or more elements in the array with length 3, requested {} values!", src.len());
                OptionalData::I3(src[0] as i64, src[1] as i64, src[2] as i64)
            },
        }
    }
}

impl<'a> From<&'a [i32]> for OptionalData {
    fn from (src: &[i32]) -> OptionalData {
        match src.len() {
            0 => {
                OptionalData::NoData
            },
            1 => {
                OptionalData::I(src[0] as i64)
            },
            2 => {
                OptionalData::I2(src[0] as i64, src[1] as i64)
            },
            3 => {
                OptionalData::I3(src[0] as i64, src[1] as i64, src[2] as i64)
            },
            _ => {  // four or more
                error!("Four or more elements in the array with length 3, requested {} values!", src.len());
                OptionalData::I3(src[0] as i64, src[1] as i64, src[2] as i64)
            },
        }
    }
}

impl<'a> From<&'a [i64]> for OptionalData {
    fn from (src: &[i64]) -> OptionalData {
        match src.len() {
            0 => {
                OptionalData::NoData
            },
            1 => {
                OptionalData::I(src[0])
            },
            2 => {
                OptionalData::I2(src[0], src[1])
            },
            3 => {
                OptionalData::I3(src[0], src[1], src[2])
            },
            _ => {  // four or more
                OptionalData::I3(src[0], src[1], src[2])
            },
        }
    }
}

impl<'a> From<&'a [f64]> for OptionalData {
    fn from (src: &[f64]) -> OptionalData {
        match src.len() {
            0 => {
                OptionalData::NoData
            },
            1 => {
                OptionalData::F(src[0])
            },
            2 => {
                OptionalData::F2(src[0], src[1])
            },
            3 => {
                OptionalData::F3(src[0], src[1], src[2])
            },
            _ => {  // four or more
                OptionalData::F3(src[0], src[1], src[2])
            },
        }
    }
}


/// single measurement of electric parameter. For example, for Schneider iEM3155 current is
/// measured as 3 phases and mean values. But we must cover all possible variatns so I prefer
/// a hashmap for these data.
/// Тип введён для более красивого JSON
#[derive(Debug, Serialize)]
pub enum Measurement {
    Energy(HashMap<String, OptionalData>),
    Current(HashMap<String, OptionalData>),
    Power(HashMap<String, OptionalData>),
    Voltage(HashMap<String, OptionalData>),
}

/// Parameter is a builder for Measurement structure
#[derive(Debug)]
pub struct Parameter {
    name: String,
    data: HashMap<String, OptionalData>,
}

impl Parameter {
    pub fn new(name_new: &str) -> Self {
        Parameter {
            name: name_new.to_string(),
            data: HashMap::new(),
        }
    }

    pub fn add(mut self, name: &str, od: OptionalData) -> Parameter {
        let name_as_string = name.to_string();
        if self.data.contains_key(&name_as_string) {
            error!("Duplicate key {} in parameter {}", name, self.name);
        }
        else {
            self.data.insert(name_as_string, od);
        }
        self
    }

    pub fn fix(self) -> Option<Measurement> {
        match self.name.to_lowercase().as_str() {
            "power"  |"powers"   => Some(Measurement::Power(self.data)),
            "current"|"currents" => Some(Measurement::Current(self.data)),
            "energy" |"energies" => Some(Measurement::Energy(self.data)),
            "voltage"|"voltages" => Some(Measurement::Voltage(self.data)),
            _ => None,
        }
    }
}


#[derive(Debug, Serialize)]
pub struct ElectricMeter {
    name: String,
    data: Vec<Measurement>,
}

pub struct ElectricMeterBuilder {
    name: String,
    data: Vec<Measurement>,
}

impl ElectricMeterBuilder {
    pub fn new(name_new: &str) -> Self {
        ElectricMeterBuilder { name: name_new.to_string(), data: Vec::new() }
    }

    pub fn set_name (mut self, name: &str) -> Self {
        self.name = name.to_string();
        self
    }

    pub fn add_data(mut self, m: Measurement) -> ElectricMeterBuilder {
        self.data.push(m);
        self
    }

    pub fn fix(self) -> ElectricMeter {
        ElectricMeter {
            name: self.name,
            data: self.data,
        }
    }
}

#[test]
fn test_constructor() {
    let curr = Parameter::new("current")
        .add("Phase", OptionalData::F3(23.45, 54.234, 12.45))
        .add("Mean", OptionalData::F(26.235));
    let em = ElectricMeterBuilder::new("Just a test struct")
        .add_data(curr)
        .fix();
    assert_eq!(em.data[0].get("current").get(Mean), 26.235);
    println!("{:?}", em);
}
