// from CLAP's example
//
// extern crate clap;

use clap::{Arg, App};
use std::str::FromStr;

// All CLI defaults are string constants, numbers too
// const DEFAULT_MODEL :String = "iEM3155";
const DEFAULT_MODEL   :&'static str = "iEM3155";
const DEFAULT_PORT    :u16 = 502;
const DEFAULT_SLAVEID :u16 = 1;

#[derive(Debug)]
pub struct SettingsStruct {
  ip        :String ,
  model     :String ,
  opt_s_logfile: Option<String>,
  tcp_port  :u16 ,
  mb_slave  :u16 ,
  verb_lvl  :u8 ,
}

impl SettingsStruct {
    pub fn new() -> SettingsStruct 
    {
        SettingsStruct {
            ip: String::with_capacity(16),
            model: String::with_capacity(50),
            opt_s_logfile: None,
            tcp_port: DEFAULT_PORT,
            mb_slave: DEFAULT_SLAVEID,
            verb_lvl: 0,
        }
    }

    // Accessors for information
    pub fn model(&self) -> &str { &self.model }

    pub fn ip(&self) -> &str { &self.ip }

    pub fn log_file(&self) -> Option<String> {self.opt_s_logfile.clone()}

    pub fn port(&self) -> u16 { self.tcp_port }

    pub fn port_i32(&self) -> i32 { self.tcp_port as i32 }

    pub fn slaveid(&self) -> u16 { self.mb_slave }

    pub fn slaveid_u8(&self) -> u8 { self.mb_slave as u8 }

    pub fn verbosity(&self) -> u8 { self.verb_lvl }

    // fn process_args(settings: &mut SettingsStruct) -> Option<()> {
    pub fn process_args() -> Option<SettingsStruct> {
        let mut settings = SettingsStruct::new();
        let s_def_port = DEFAULT_PORT.to_string();
        let s_def_slave = DEFAULT_SLAVEID.to_string();
        let matches = App::new("Request information from Modbus-equipped electric meter")
                              .version("1.0")
                              .author("Dmitry Golub <d.golub@hostco.ru>, HOST IS LLC")
                              .about("Requests an information from electric meters for Zabbix (output as JSON to std.out)")
                              .arg(Arg::with_name("model")
                                   .short("m")
                                   .long("model")
                                   .value_name("Model")
                                   .help("Sets a model of electric meter (YAML file with parameters to request)")
                                   .takes_value(true)
                                   .required(false))

                              .arg(Arg::with_name("ip")
                                   .long("ip")
                                   .value_name("IP.ADD.RE.SS")
                                   .help("Sets the IP address of ModBus device")
                                   .takes_value(true)
                                   .required(true))

                              .arg(Arg::with_name("port")
                                   .long("port")
                                   .short("p")
                                   .value_name("TCP_Port_Number")
                                   .help("Sets the TCP port of ModBus device")
                                   .takes_value(true)
                                   .required(false))

                              .arg(Arg::with_name("slave")
                                   .long("slave_id")
                                   .short("s")
                                   .value_name("ModBus_Slave_ID")
                                   .help("Sets the slave ID of ModBus device to request information from")
                                   .takes_value(true)
                                   .required(false))

                              .arg(Arg::with_name("log_file")
                                    .long("log_file")
                                    .short("l")
                                    .value_name("log_file")
                                    .help("Log file name, default = <stderr>")
                                    .takes_value(true)
                                    .required(false))

                              .arg(Arg::with_name("v")
                                   .short("v")
                                   .multiple(true)
                                   .help("Sets the level of verbosity"))
                              .get_matches();

        // Gets a value for model if supplied by user, or defaults to constant 
        let s_model = matches.value_of("model").unwrap_or(DEFAULT_MODEL);
        settings.model = s_model.to_string();

        // Calling .unwrap() is safe here because "ip" is required (if "ip" wasn't
        // required we could have used an 'if let' to conditionally get the value)
        let s_ip = matches.value_of("ip").unwrap();
        settings.ip = s_ip.to_string();

        
        settings.opt_s_logfile = matches.value_of("log_file").map(|s| s.to_string());

        // Gets a value for TCP port if supplied by user, or defaults to constant 
        let s_port = (matches.value_of("port").unwrap_or(&s_def_port)).to_string();
        if let Ok(p) = u16::from_str(&s_port) {
            settings.tcp_port = p;
        } else {
            println!("Cannot parse port number as integer");
            return None
        }

        // Gets a value for Modbus Slave ID if supplied by user, or defaults to constant (1)
        let s_slave = (matches.value_of("slave").unwrap_or(&*s_def_slave)).to_string();
        settings.mb_slave = u16::from_str(&s_slave).expect("Cannot parse slave ID as integer");
        assert!(settings.mb_slave < 255);

        // Vary the output based on how many times the user used the "verbose" flag
        // (i.e. 'myprog -v -v -v' or 'myprog -vvv' vs 'myprog -v'
        match matches.occurrences_of("v") {
            0     => { settings.verb_lvl = 0},
            1     => { settings.verb_lvl = 1},
            2     => { settings.verb_lvl = 2},
            3 | _ => { settings.verb_lvl = 3},
        }
        Some(settings)
    }

    fn print(settings: &SettingsStruct) {
        println!("{:?}", settings);
    }
}
