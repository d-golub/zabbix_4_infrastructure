/// Программа для сбора данных с электрических счётчиков по протоколу Modbus/TCP.
/// Работает в паре с Zabbix, выдаёт результат на стандартный вывод в виде JSON объекта.
/// Программе указывается через CLI: 
///  - модель счётчика (YAML файл с описанием), 
///  - параметры Modbus-TCP (IP, порт, Slave ID).

// for logging
extern crate chrono;   // for timestamp in a log line
extern crate num;      // convertions btw numeric types etc
#[macro_use] extern crate log;      // general logging support 
extern crate fern;     // will log with fern (https://dabo.guru/rust/fern/fern/)

extern crate clap;     // CLI arguments and options
extern crate yaml_rust;

extern crate serde;
extern crate serde_json;
#[macro_use] extern crate serde_derive;
#[macro_use] extern crate failure_derive;
#[macro_use] extern crate failure;

// ModBus implementation (interface to libmodbus)
extern crate libmodbus_sys;
extern crate libmodbus_rs;


// these modules are defined here, in this project crate
mod modbus_types;
mod cli_args;
mod electric_params;
mod errors;

// numeric types, traits, conversions
// use num::Num;
// use num::cast;


// use electric_meter::cli_args::SettingsStruct;
use cli_args::SettingsStruct;
use std::env;
// use std::collections::HashMap;
use std::path::{Path, PathBuf};
// for file I/O
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::OpenOptions;
use yaml_rust::{YamlLoader, Yaml};
use modbus_types::ModBusDataTypes;
use electric_params::{ElectricMeterBuilder, ElectricMeter, OptionalData, Parameter, Measurement};
use errors::EMError;
use libmodbus_rs::{Modbus, ModbusTCP, ModbusClient};
/* use failure; */

type Error = failure::Error;

//constants
/* const MODELS_DB_DIR :&'static str = "/usr/share/electric_meter/meters"; */
/* const MODELS_DB_DIR :&'static str = "/home/dgolub/_Workspace/_Projects_/zabbix4infra/Rust/electric_meter/Model"; */
const MODELS_DB_DIR :&'static str = "/home/dgolub/_Projects_/Zabbix4Infra/Rust/electric_meter/Model";

fn main() {
    if let Some(settings) = SettingsStruct::process_args() {
        // set up logging
       match setup_logger(&settings) {
           Err(e) => writeln!(std::io::stderr(), "Error setting up logger: {}", e).unwrap(),
           Ok(_) => {
               debug!("Settings: {:?}", settings);

               match request_info(&settings) {
                   Err(e) => {
                       error!("Error getting data from electric meter");
                       error!("request_info returned {}", e);
                   },
                   Ok(json_data) => {
                       println!("{}", json_data);
                   }
               }
           },
        }
    }
}

/// Logging setup.
/// Parameters: 1) Configuration data (structure) -- used verbosity level and log file name.
/// Returns: Fern::Result, Ok response is empty
fn setup_logger(cfg: &SettingsStruct) -> std::result::Result<(), fern::InitError> {
    let mut base_config = fern::Dispatch::new();
    base_config = match cfg.verbosity() {
        0 => base_config.level(log::LevelFilter::Error),
        1 => base_config.level(log::LevelFilter::Info),
        2 => base_config.level(log::LevelFilter::Debug),
        3 | _ => base_config.level(log::LevelFilter::Trace),
    };

    match cfg.log_file() {
        Some(fname) => {
            // Separate file config so we can include year, month and day in file logs
            let file_config = fern::Dispatch::new()
                .format(|out, message, record| {
                    out.finish(format_args!(
                        "{}[{}][{}] {}",
                        chrono::Local::now().format("[%Y-%m-%d__%H:%M:%S]"),
                        record.target(),
                        record.level(),
                        message
                    ))
                })
                .chain(fern::log_file(fname)?);

            base_config.chain(file_config).apply()?;
        },
        None => {
            let stderr_config = fern::Dispatch::new()
                .format(|out, message, record| {
                    // special format for debug messages coming from our own crate.
                    out.finish(format_args!(
                        "[{}][{}][{}] {}",
                        chrono::Local::now().format("%H:%M:%S"),
                        record.target(),
                        record.level(),
                        message
                    ))
                })
                .chain(std::io::stderr());

            base_config.chain(stderr_config).apply()?;
        },
    }
    Ok(())
}

/// функция получает ссылку на структуру с настройками (из CLI) и в соответствии с ними делает:
///  - ищет модель счётчика как YAML файл в заданном каталоге 
///  - вызывает функцию, которая парсит это описание и, руководствуясь им, опрашивает
///    счётчик, возвращая данные как JSON строку в контейнере Result
fn request_info(settings :&SettingsStruct) -> Result<String, Error> {
    // Ищем описание счётчика в базе моделей
    if let Some(s_features) = search_for_model(settings.model()) {
        debug!("Features file: {}", s_features);
        // Читаем описание из файла
        return read_model_file(&s_features)
            .and_then(|mod_feat|
                      parse_model(mod_feat, settings)
            )
    }
    error!("Can't find model description file");
    bail!(EMError::CannotFindModel("Cannot find model description file"))
}

fn search_for_model(model :&str) -> Option<String> {
    //! поиск модели. Используется переменная среды "MODELS_DB_DIR", 
    //! если её нет -- глобальная константа MODELS_DB_DIR
    //! Если модель найдена, возвращается имя файла, где лежит её описание,
    //! если не найдена -- None.
    trace!("search_for_model: requested model {}", model);
    let db_dir  = env::var("MODELS_DB_DIR");    // Result
    debug!("Environment variable MODELS_DB_DIR: {:?}", db_dir);
    let db_dir = match db_dir {
        Ok(env_value) => env_value,
        Err(_) => MODELS_DB_DIR.to_string(),
    };
    // construct full yaml description filename from components: db_dir, model, '.yaml' extension
    let mut model_file_path = PathBuf::from(&db_dir);
    model_file_path.push(&model);
    model_file_path.set_extension("yaml");

    trace!("Searching for file {:?}", model_file_path);
    // if this file exists?
    let path_as_string = model_file_path.clone().into_os_string().into_string();
    match path_as_string {
        Ok(path_as_string) => {
            if Path::new(&model_file_path).is_file() {
                debug!("found file {} with a model description", path_as_string);
                Some(path_as_string)
            } else {
                error!("Cannot find file {} with a given model name", path_as_string);
                None
            }
        },
        Err(path_as_string) => {
            // let filename :String = db_dir.to_string() + "/" + model + ".yaml";
            error!("Invalid file name {:?} for your operating system", path_as_string);
            None
        },
    }
}

/// Чтение YAML файла с описанием модели счётчика. На входе имя файла,
/// строка уже проверена на то, что это правильное имя файла и этот файл существует.
/// Возвращает YAML строку (содержание файла) или None, если файл нельзя прочесть.
fn read_model_file(fullname: &str) -> Result<String, Error> {
    if let Ok(f_in) = OpenOptions::new().read(true).open(fullname) {
        let mut f_in = BufReader::new(f_in);
        // 1KB auto-extended string buffer for file contents
        let mut s_contents = String::with_capacity(1024);
        if let Ok(_res) = f_in.read_to_string(&mut s_contents) {
            Ok(s_contents)
        } else {
            error!("Can't read file {} to string", fullname);
            bail!(EMError::CannotReadModelFile("Cannot read model description file"))
        }
    } else {
        // Error message
        error!("Can't open file with model description for reading");
        bail!(EMError::CannotOpenModelFile("Cannot open model description file for reading"))
    }
}

/// Обработка файла с описанием модели. На входе строка с YAML данными,
/// на выходе должна быть JSON строка такой же структуры, но вместо описания
/// полей должны быть подставлены данные:
/// Вход:
///  >> ... current: { L1: {reg: 3000, type: f32}, L2: {reg:3002, type: f32} }
/// Выход:
///  << ... current: { L1: 23.123, L2: 22.838 }
/// Вход: 
///  >> ... current: {type: f32, Phase: [3000, 3002, 3004]}
/// Выход: 
///  << ... current: {Phase: [23.123, 22.838, 23.167]}
fn parse_model(yml_data: String, settings :&SettingsStruct) -> Result<String, Error> {
    // if let Ok(tree_vec) = 
    YamlLoader::load_from_str(&yml_data)
        .map_err(|e| format_err!("Error '{}' loading YAML file", e))
        .and_then (|tree_vec: Vec<yaml_rust::Yaml>| {
            debug!("Source Yaml tree: {:?}", tree_vec);
            debug!("Length of Yaml tree vector: {:?}", tree_vec.len());
            if tree_vec.len() == 1 {
                parse_yaml_node(&tree_vec[0], &settings)
            } else {
                // incorrect YAML file
                error!("Incorrect YAML description, more than one top-level object");
                bail!(EMError::InvalidYamlFile("Too many top-level objects"))
            }
        })
}

fn mb_read_i16(modbus :&Modbus, addr :i64) -> Result<i16, Error> {
    const I_REGISTERS_TO_READ: u16 = 1;
    let mut response_registers = vec![0u16; I_REGISTERS_TO_READ as usize];
    match modbus.read_input_registers(addr as u16, I_REGISTERS_TO_READ, &mut response_registers) {
        // direct case-analysis here for now.
        Ok(1) => Ok(response_registers[0] as i16),
        Ok(_) => bail!(EMError::IncorrectNumWordsRead("Incorrect number of words in ModBus reply")),
        Err(e) => bail!(e.to_string()),
    }
}

fn mb_read_i32(modbus :&Modbus, addr: i64) -> Result<i32, Error> {
    Ok(42_i32)
}

fn mb_read_i64(modbus :&Modbus, addr: i64) -> Result<i64, Error> {
    Ok(42_i64)
}

fn mb_read_f32(modbus :&Modbus, addr: i64) -> Result<f32, Error> {
    const I_REGISTERS_TO_READ: u16 = 2;
    let mut response_registers = vec![0u16; I_REGISTERS_TO_READ as usize];
    match modbus.read_input_registers(addr as u16, I_REGISTERS_TO_READ, &mut response_registers) {
        // direct case-analysis here for now.
        Ok(1) => Ok(reg_to_f32(response_registers[0])),
        Ok(_) => bail!(EMError::IncorrectNumWordsRead("Incorrect number of words in ModBus reply")),
        Err(e) => bail!(e.to_string()),
    }
}

fn mb_read_f64(modbus :&Modbus, addr: i64) -> Result<f64, Error> {
    Ok(42_f64)
}


/// Чтение простых (регистровых) данных заданного формата из ModBus. Возвращает OptionalData
/// со простым значением заказанного типа (I или F). Примерно так:
/// Тип задаётся вариантом Enum-а ModBusDataTypes.
/// read_from_modbus_addr(3000, F32) -> OptionalData::F(21.344_f64)
/// read_from_modbus_addr(3656, I64) -> OptionalData::I(8623411_i64)
fn read_from_modbus_addr(mb: &Modbus, addr: i64, datatype: ModBusDataTypes) -> Result<OptionalData, Error>
{
    trace!("read_from_modbus_addr: will read {1} from {0}", addr, datatype);
    let res = match datatype {
        ModBusDataTypes::I16 => Ok(OptionalData::from(mb_read_i16(mb, addr))),
        ModBusDataTypes::I32 => Ok(OptionalData::from(mb_read_i32(mb, addr))),
        ModBusDataTypes::I64 => Ok(OptionalData::from(mb_read_i64(mb, addr))),
        ModBusDataTypes::F32 => Ok(OptionalData::from_res_float(mb_read_f32(mb, addr))),
        ModBusDataTypes::F64 => Ok(OptionalData::from_res_float(mb_read_f64(mb, addr))),
        _ => {
            error!("Invalid ModBus data type");
            bail!(EMError::UnknownDataTypeError("Cannot read unknown data type"))
        },
    };
    debug!("return from ModBus: {:?}", res);
    res
}


/// чтение из ModBus группы значений из последовательных адресов (во всяком случае, должна
/// быть возможность чтения этих адресов одной операцией). Практический лимит -- двадцать-тридцать
/// значений.
fn read_array_from_mbus(addr :&Vec<yaml_rust::Yaml>, datatype :ModBusDataTypes) -> Result<OptionalData, Error> {
    debug!("read_array_from_mbus called with array: {:?} of type {}", addr, datatype);
    let mut addresses = Vec::new();
    for elem in addr {
        match *elem {
            Yaml::Integer(reg) => addresses.push(reg),
            _ => { 
                error!("Invalid address {:?} occurs in read_array_from_mbus", elem);
                bail!(EMError::InvalidAddressError("invalod address for ModBus reading"));
            },
        }
    }
    debug!("Addresses array: {:?}", addresses);
    // ищем  максимальный и минимальный адреса
    let max_addr = addresses.iter().fold(addresses[1], |max, &v| {if v > max {v} else {max}});
    let min_addr = addresses.iter().fold(addresses[1], |min, &v| {if v < min {v} else {min}});
    debug!("Range of addresses from {} to {}", min_addr, max_addr);
    Ok(OptionalData::F3(42.1, 42.2, 42.3))
}

/// fn read_pair_mbus()

/// разбор одной группы измерений (энергия, ток, напряжение, etc)
/// параметры: имя для включения в вывод, соответствующее значение (hash table).
/// Если в хэше на верхнем уровне задан тип, то он будет одинаков для всех значений
/// и значения будут задаваться адресом {name: register} или массивом адресов 
/// {Phase: [register, register, ...]}
/// 
/// XXX надо избавиться от кучи паников в этой функции и вообще переделать её в человеческий вид.
fn parse_measure(mb: &Modbus, name: &str, node: &Yaml, setting:&SettingsStruct) -> Result<Measurement, Error> {
    debug!("parse_measure called with name {} and value {:?}", name, node);
    let mut default_t = ModBusDataTypes::from_str("i32").unwrap();
    let mut param_builder = Parameter::new(name);
    if let Yaml::Hash(ref ht) = *node {
        for (k,v) in ht {
            debug!("key {:?}, value {:?}", k,v);
            let str_key = k.as_str().unwrap();
            match str_key {
                "type" => {   // default type of ModBus value definition
                    match ModBusDataTypes::from_str(v.as_str().unwrap()) {
                        Some(def_t) => {
                            debug!("Numeric values will belong to type {:?}", def_t);
                            default_t = def_t;
                        },
                        None => {
                            error!("Incorrect default datatype, cannot continue");
                            bail!(format_err!("Fatal error"))
                        },
                    }
                },
                _ => {   // Have we to define all the possible variants here like 'Phases' or 'LN' or 'LL'?
                    // another key, let's see the value...
                    debug!("processing key {}", str_key);
                    match *v {
                        Yaml::Integer(addr) => {
                            if let Ok(od) = read_from_modbus_addr(&mb, addr, default_t) {
                                // make a data element for return
                                debug!("One ModBus value, key={0}, returning {1:?}", str_key, od);
                                param_builder = param_builder.add(str_key, od);
                            } else {
                                error!("cannot read type {0} from ModBus address {1}", default_t, addr);
                            }
                        },
                        Yaml::Array(ref arr) => {
                            if let Ok(pd) = read_array_from_mbus(arr, default_t) {
                                param_builder = param_builder.add(str_key, pd);
                            } else {
                                error!("cannot read array of type {0} from ModBus addresses {1:?}", default_t, arr);
                            }
                        },
                        Yaml::Hash(ref h) => {
                            debug!("Hash found: {:?}", h);
                            for (k, v) in h {
                                let k = ModBusDataTypes::from_str(k.as_str().unwrap());
                                match (k, v) {
                                    (Some(typ), &Yaml::Integer(ref addr)) => {
                                        debug!("addr-type Hash!, type {0}, address {1}", typ, addr);
                                        if let Ok(val) = read_from_modbus_addr(mb, *addr, typ) {
                                            debug!("One ModBus value from hash, key={0:?}, assigning {1:?} to key {2}", k, val, str_key);
                                            param_builder = param_builder.add(str_key, val);
                                        } else {
                                            error!("Cannot read from ModBus, addr {}, type {}", addr, typ);
                                        }
                                    },
                                    (None, _) => {panic!("Invalid datatype occured in Yaml hash {:?}", h)},
                                    // XXX строка, не являющаяся Integer, проваливается в
                                    // следующий оператор с паникой.
                                    (_, _) => bail!(format_err!("Invalid address in yaml hash: {:?}", h)),
                                    _ => unreachable!(),
                                }
                            }
                        },
                        _ => bail!(format_err!("parse_measure: incorrect value of key {}", str_key)),
                    }
                },
            }
            /* dump_node(v, 0); */
        }
    }
    debug!("Param builder data: {:?}", param_builder);
    match param_builder.fix() {
        None => bail!("Incorrect return from param_builder"),
        Some(p) => Ok(p),
    }
}

/// На входе узел Yaml со внутренней структурой (описание электросчётчика).
/// На выходе -- JSON строка со считанными данными или ошибка.
/// Параметры: 
///     1) ссылка на YAML узел 2) Структура с настройками
/// Возвращает: 
///     Result, в ветке ОК - JSON строка с результатами, в ветке Err описание ошибки.
/// Делает: 
/// проверяет, что JSON на верхнем уровне -- объект (ассоциативный массив),
/// и первый ключ этого объекта -- строка "ElectricMeter". Если да, вызывает 
/// parse_electric_meter её результаты в JSON.  Если нет, возвращает ошибку.
fn parse_yaml_node(node: &Yaml, settings :&SettingsStruct) -> Result<String, Error> {
    
    match *node {
        Yaml::Hash(ref hash) => {
            // the hash MUST contain key 'ElectricMeter'
            match hash.get(&Yaml::String("ElectricMeter".to_string())) {
                None => {
                    info!("Magic word 'ElectricMeter' not found in YAML file");
                    bail!(EMError::NoMagickInFile(
                        "Key 'ElectricMeter' is not found in the file"))
                },
                Some(v) => {
                    debug!("Really, electric meter");
                    parse_electric_meter(v, &settings)
                        .map_err(|e| format_err!("Error {} in 'parse_electric_meter'", e))
                        .and_then(|em| serde_json::to_string(&em)
                            .map_err(|e| format_err!("Error '{}' converting JSON to string", e)))
                },
            }
        },
        _ => {
            // top level of Electric Meter must be Yaml object (hash)
            error!("Top level of YAML file isn't an object, cannot continue");
            bail!(EMError::InvalidYamlFile("Top level of YAML file isn't an object"))
        },
    }
}

/// Prepares and opens ModBus connection according to data from SettingsStruct
fn prepare_modbus_client(sets: &SettingsStruct) -> Result<Modbus, Error> {
    let mut o_mbclient = Modbus::new_tcp(sets.ip() , sets.port_i32());
    match o_mbclient {
        Err(_) => {
            bail!(EMError::ModbusError("Cannot open ModBus connection"))
        },
        Ok(mut o_mbclient) => {
            o_mbclient.set_slave(sets.slaveid_u8())
                    .map_err(|e| format_err!("Bad return from libmodbus::set_slave: {:?}", e))?;
            o_mbclient.set_debug(false)
                    .map_err(|e| format_err!("Bad return from libmodbus::set_debug: {:?}", e))?;
            o_mbclient.connect()
                    .map_err(|e| format_err!("Libmodbus error, cannot connect(): {:?}", e))?;
            Ok(o_mbclient)
        },
    }
}

/// Разбор параметров электрического счётчика, описанных в YAML файле, выполнение запросов по
/// ModBus, возврат структуры electric_params::ElectricMeter с заполненными данными.
fn parse_electric_meter(s_yaml: &Yaml, settings: &SettingsStruct) -> Result<ElectricMeter, Error> {
    let mut emb = ElectricMeterBuilder::new("ElectricMeter");
    if let Ok(o_mbclient) = prepare_modbus_client(&settings) {
        match *s_yaml {
            Yaml::Hash(ref hash) => {
                for (k, v) in hash {
                    let key_name :&str = k.as_str().unwrap();
                    match key_name { 
                        "energy" | "current" | "currents" | "voltage" | "voltages" | 
                            "power" | "powers" => {
                            // group of measuments (possibly by phase)
                            debug!("Begin of measuments group {}", key_name);
                            // let Ok(m) = parse_measure(&o_mbclient, key_name, &v, &settings)?;
                            let m = parse_measure(&o_mbclient, key_name, &v, &settings)?;
                            debug!("Ok from parse_measure for key {}, data={:?}", key_name, m);
                            emb = emb.add_data(m);
                            /*
                            if let Ok(m) = parse_measure(&o_mbclient, key_name, &v, &settings) {
                                debug!("Ok from parse_measure for key {}, data={:?}", key_name, m);
                                emb = emb.add_data(m);
                            }
                            */
                        },
                        _ => {
                            debug!("other key '{}' and value {:?}, do nothing", key_name, v);
                        },
                    }
                } 
            },
            _ => {
                error!("Incorrect structure of Yaml file: {:?} is not a hash", s_yaml);
            },
        }
        // -- XXX close ModBus connection --
        Ok(emb.fix())
    } else {
        bail!(EMError::ModbusError("Cannot connect with Modbus"))
    }
}
