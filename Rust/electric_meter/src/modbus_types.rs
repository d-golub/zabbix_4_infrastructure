// will log with fern (https://dabo.guru/rust/fern/fern/)

use std::fmt::{Display, Formatter, Result};

#[derive(Debug, Clone, Copy)]
pub enum ModBusDataTypes {
    I16,
    I32,
    I64,
    F32,
    F64,
}

impl ModBusDataTypes {
    pub fn from_str(s: &str) -> Option<ModBusDataTypes> {
        match s {
            "i16" => Some(ModBusDataTypes::I16),
            "i32" => Some(ModBusDataTypes::I32),
            "i64" => Some(ModBusDataTypes::I64),
            "f32" => Some(ModBusDataTypes::F32),
            "f64" => Some(ModBusDataTypes::F64),
            _ => { error!("ModBusDataType::from_str: Incorrect value {:?} to convert", s); None },
        }
    }
    
}

impl Display for ModBusDataTypes {
    fn fmt(&self, f: &mut Formatter) -> Result {
        let myself_str = match *self {
            ModBusDataTypes::I16 => "i16" ,
            ModBusDataTypes::I32 => "i32" ,
            ModBusDataTypes::I64 => "i64" ,
            ModBusDataTypes::F32 => "f32" ,
            ModBusDataTypes::F64 => "f64" ,
        };
        write!(f, "{}", myself_str)
    }
}
