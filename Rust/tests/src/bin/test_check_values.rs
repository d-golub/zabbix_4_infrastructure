// Проверка корректности работы плагина Modbus в Заббиксе
// Вход: последовательность строк вида:
// 2018-10-11 19:16:53 1539267413 {"2999":27.603262,"3001":43.869289,"3003":35.648876,"3009":35.707142}
// 2018-10-11 19:16:38 1539267398 {"2999":28.206600,"3001":44.492043,"3003":36.423054,"3009":36.373898}
// Задача: в каждой строке проверить, что среднее арифметическое значений первых трёх полей равно
// значению четвёртого поля.

#[macro_use]
extern crate serde_derive;

extern crate serde;
extern crate serde_json;

const EPSILON: f32 = 0.1E-6;

#[derive(Serialize, Deserialize, Debug)]
#[serde(untagged)]
enum RegisterData {
    PairF { id: String, val: f32 },
    PairI { id: String, val: i32 },
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(untagged)]
enum CurrentData {
    Four (RegisterData, RegisterData, RegisterData, RegisterData),
}

impl CurrentData {
    fn getFloat32(reg_data: &RegisterData) -> f32 {
            match reg_data {
                RegisterData::PairF{id, val} => *val,
                RegisterData::PairI{id, val} => *val as f32
            }
    }

    fn l1f(&self) -> f32 {
        match self {
            CurrentData::Four (l1, _, _, _) => CurrentData::getFloat32(l1)
        }
    }

    fn l2f(&self) -> f32 {
        match self {
            CurrentData::Four (_, l2, _, _) => CurrentData::getFloat32(l2)
        }
    }

    fn l3f(&self) -> f32 {
        match self {
            CurrentData::Four (_, _, l3, _) => CurrentData::getFloat32(l3)
        }
    }

    fn avgf(&self) -> f32 {
        match self {
            CurrentData::Four (_, _, _, avg) => CurrentData::getFloat32(avg)
        }
    }


    fn check(&self) -> bool {
        let myavg = self.l1f() + self.l2f() + self.l3f() ;
        let diff = myavg / 3.0 - self.avgf();
        if diff.abs() < EPSILON {
            println!("difference {}  is less than Epsilon {}", diff, EPSILON);
            true
        } else {
            false
        }
    }
}


fn main() {
    let d = CurrentData::Four(
                RegisterData::PairF{id: "1".to_string(), val: 28.206600}, 
                RegisterData::PairF{id: "2".to_string(), val: 44.492043}, 
                RegisterData::PairF{id: "3".to_string(), val: 36.423054},
                RegisterData::PairF{id: "4".to_string(), val: 36.373898},
            );
    println!("{:?}: {}", d, d.check());
    println!("{}", serde_json::to_string(&d).unwrap());
    let d : CurrentData = serde_json::from_str("{\"2999\":27.603262,\"3001\":43.869289,\"3003\":35.648876,\"3009\":35.707142}").unwrap();
    println!("{:?}", d);
}
