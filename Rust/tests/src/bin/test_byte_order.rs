extern crate bytes;
extern crate byteorder;

use bytes::{Bytes, BytesMut, Buf, BufMut, IntoBuf};
use byteorder::ByteOrder;
use byteorder::LittleEndian as LE;
use byteorder::BigEndian as BE;
use byteorder::NativeEndian as NE;
use std::mem::{size_of, swap};

enum Endianess { BE, LE, NE}

/// A group of functions 
mod SwapBytes {
    fn byteorder_cbda_to_abcd(a: &[u8;4]) -> &[u8;4] {
        a.to_bytes()
    }

}


fn main() {
    // Initialize variables, create buffer
    const E :Endianess = Endianess::LE;

    let v_ibuf = vec![16886_i16, -11667_i16, 2345_i16, 32354_i16];
    let mut mbuf = BytesMut::with_capacity(size_of::<f64>() + 2);

    for i in v_ibuf {
        match E {
            Endianess::BE => mbuf.put_i16::<BE>(i),
            Endianess::LE => mbuf.put_i16::<LE>(i),
            Endianess::NE => mbuf.put_i16::<NE>(i),
        }
    }
    println!("i16 buffer: converts to {:?} bytes", mbuf);
    
    // now read data from buffer
    // println!("buffer length: {}", mbuf.len());
    assert_eq!(mbuf.len(), 8);

    // println!("Conversion to f32:{}", &mbuf.into_buf().get_f32::<BigEndian>());
    println!("Conversion to f64:{}", match E {
        Endianess::BE => mbuf.into_buf().get_i64::<BE>(),
        Endianess::LE => mbuf.into_buf().get_i64::<LE>(),
        Endianess::NE => mbuf.into_buf().get_i64::<NE>(),
    });
}
