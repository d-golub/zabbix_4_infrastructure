// (Full example with detailed comments in examples/01quick_example.rs)
//
// This example demonstrates clap's full 'builder pattern' style of creating arguments which is
// more verbose, but allows easier editing, and at times more advanced options, or the possibility
// to generate arguments dynamically.
extern crate clap;
use clap::{Arg, App};
use std::str::FromStr;

// All CLI defaults are string constants, numbers too
// const DEFAULT_MODEL :String = "iEM3155";
const DEFAULT_MODEL   :&'static str = "iEM3155";
const DEFAULT_PORT    :u16 = 502;
const DEFAULT_SLAVEID :u16 = 1;

#[derive(Debug)]
struct SettingsStruct {
  ip        :String ,
  model     :String ,
  tcp_port  :u16 ,
  mb_slave  :u16 ,
  verb_lvl  :u8 ,
}

impl SettingsStruct {
    fn new() -> SettingsStruct 
    {
        SettingsStruct {
            ip: String::from(""),
            model: String::from(DEFAULT_MODEL),
            tcp_port: DEFAULT_PORT,
            mb_slave: DEFAULT_SLAVEID,
            verb_lvl: 0,
        }
    }

}
// fn process_args(settings: &mut SettingsStruct) -> Option<()> {
fn process_args() -> Option<SettingsStruct> {
    let mut settings = SettingsStruct::new();
    let s_def_port = DEFAULT_PORT.to_string();
    let s_def_slave = DEFAULT_SLAVEID.to_string();
    let matches = App::new("Test of command line arguments processing")
                          .version("1.0")
                          .author("Dmitry Golub <d.golub@hostco.ru>, based on example of Kevin K. <kbknapp@gmail.com>")
                          .about("Requests an information from electric meters for Zabbix")
                          .arg(Arg::with_name("model")
                               .short("m")
                               .long("model")
                               .value_name("Model")
                               .help("Sets a model of electric meter (YAML file with parameters to request)")
                               .takes_value(true)
                               .required(false))

                          .arg(Arg::with_name("ip")
                               .long("ip")
                               .value_name("IP.ADD.RE.SS")
                               .help("Sets the IP address of ModBus device")
                               .takes_value(true)
                               .required(true))

                          .arg(Arg::with_name("port")
                               .long("port")
                               .short("p")
                               .value_name("TCP_Port_Number")
                               .help("Sets the TCP port of ModBus device")
                               .takes_value(true)
                               .required(false))

                          .arg(Arg::with_name("slave")
                               .long("slave_id")
                               .short("s")
                               .value_name("ModBus_Slave_ID")
                               .help("Sets the slave ID of ModBus device to request information from")
                               .takes_value(true)
                               .required(false))

                          .arg(Arg::with_name("v")
                               .short("v")
                               .multiple(true)
                               .help("Sets the level of verbosity"))
                          .get_matches();

    // Gets a value for model if supplied by user, or defaults to constant 
    let s_model = matches.value_of("model").unwrap_or(DEFAULT_MODEL);
    println!("Electic meter model: {:?}", s_model);
    settings.model = s_model.to_string();

    // Calling .unwrap() is safe here because "ip" is required (if "ip" wasn't
    // required we could have used an 'if let' to conditionally get the value)
    let s_ip =matches.value_of("ip").unwrap();
    println!("Gateway IP: {}", s_ip);
    settings.ip = s_ip.to_string();

    // Gets a value for TCP port if supplied by user, or defaults to constant 
    let s_port = (matches.value_of("port").unwrap_or(&s_def_port)).to_string();
    // let s_port = (matches.value_of("port").unwrap_or(&*s_def_port)).to_string();
    println!("TCP Port #: {}", s_port);
    if let Ok(p) = u16::from_str(&s_port) {
        settings.tcp_port = p;
    } else {
        println!("Cannot parse port number as integer");
        return None
    }


    // Gets a value for Modbus Slave ID if supplied by user, or defaults to constant (1)
    let s_slave = (matches.value_of("slave").unwrap_or(&*s_def_slave)).to_string();
    println!("Modbus Slave #: {}", s_slave);
    settings.mb_slave = u16::from_str(&s_slave).expect("Cannot parse slave ID as integer");
    assert!(settings.mb_slave < 255);

    // Vary the output based on how many times the user used the "verbose" flag
    // (i.e. 'myprog -v -v -v' or 'myprog -vvv' vs 'myprog -v'
    match matches.occurrences_of("v") {
        0 => { println!("No verbose info"); settings.verb_lvl = 0},
        1 => { println!("Some verbose info"); settings.verb_lvl = 1},
        2 => { println!("Tons of verbose info"); settings.verb_lvl = 2},
        3 | _ => { println!("Don't be crazy"); settings.verb_lvl = 2},
    }
    Some(settings)
}

fn go(settings: &SettingsStruct) {
    println!("{:?}", settings);
}

/// Process command-line arguments and call the real working function
/// Args: None
/// Returns: None (exit status to OS wia sys::process::exit() if some error occurs
/// 
fn main() {
    if let Some(settings_struct) = process_args() {
        go(&settings_struct);
        // more program logic goes here...
    } else {
        println!("Error in command-line parsing");
    }
}
