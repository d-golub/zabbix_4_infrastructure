// Проверка корректности работы плагина Modbus в Заббиксе
// Вход: последовательность строк вида:
// 2018-10-11 19:16:53 1539267413 {"2999":27.603262,"3001":43.869289,"3003":35.648876,"3009":35.707142}
// 2018-10-11 19:16:38 1539267398 {"2999":28.206600,"3001":44.492043,"3003":36.423054,"3009":36.373898}
// Задача: в каждой строке проверить, что среднее арифметическое значений первых трёх полей равно
// значению четвёртого поля.

#[macro_use]
extern crate serde_derive;

extern crate serde;
extern crate serde_json;

const EPSILON: f32 = 0.1E-6;

#[derive(Debug, Serialize, Deserialize)]
struct CurrentData {
    #[serde(rename="2999")]
    l1: f32,
    #[serde(rename="3001")]
    l2: f32,
    #[serde(rename="3003")]
    l3: f32,
    #[serde(rename="3009")]
    avg: f32,
}

impl CurrentData {
    fn check(&self) -> bool {
        let myavg = self.l1 + self.l2 + self.l3 ;
        let diff = myavg / 3.0 - self.avg;
        diff.abs() < EPSILON
    }
}

#[test]
fn check_current_data() {
    let d : CurrentData = serde_json::from_str("{\"2999\":27.603262,\"3001\":43.869289,\"3003\":35.648876,\"3009\":35.707142}").unwrap();
    assert!(d.check());
}

#[derive(Debug)]
enum ParseError {
    NoSplits,
    NoTime,
    NoCTime,
    NoJSON,
    JsonError(serde_json::Error),
}

use std::io::Result;
fn process_line(line: &str) -> std::result::Result<(), ParseError> {
    // one line: collect fields, parse JSON, check correctness of sum
    let mut it_splits = line.splitn(4, ' ');
    let date  = it_splits.next().ok_or(ParseError::NoSplits)?;
    let time  = it_splits.next().ok_or(ParseError::NoTime)?;
    let ctime = it_splits.next().ok_or(ParseError::NoCTime)?;
    let json  = it_splits.next().ok_or(ParseError::NoJSON)?;
    let cd: CurrentData = serde_json::from_str(json).map_err(|e| ParseError::JsonError(e))?;
    print!("{}, {}, {}", date, time, json);
    if cd.check() { println!("...  Ok"); } else { println!("...  Error!"); };
    Ok(())
}

enum StdinProcessError {
    IOError(std::io::Error),
    PError(ParseError),
}

use std::io::prelude::*;
use std::io::stdin;
use std::io::BufReader;
use std::fs::File;

fn process_stdin() -> std::result::Result<(), ParseError> {
    // read stdin and call process_line for each line of data
    let in_stream = BufReader::new(stdin());
    let  lines_iter = in_stream.lines().map(|l| l.unwrap());
    for l in lines_iter {
        process_line(&l).map_err(|e| return(e));
    }
    Ok(())
}

fn main() {
    match process_stdin() {
        Ok(_) => {},
        Err(_) => return,
    }
}
