// external dependencies
#[macro_use]
extern crate serde_derive;

extern crate serde;
extern crate serde_yaml;
extern crate serde_json;


// --- data  types ---
/// Electric current, by phase in 3-phase system
#[derive(Serialize, Deserialize, Debug)]
struct Current3Ph {
    r_l1: f32,
    r_l2: f32,
    r_l3: f32,
}

/// Difference  of electric potentials (voltage)
/// 
#[derive(Serialize, Deserialize, Debug)]
struct Voltage3Ph {
    r_l1_n: f32,
    r_l2_n: f32,
    r_l3_n: f32,
    r_l1_l2: f32,
    r_l2_l3: f32,
    r_l3_l1: f32,
}

/// Electric meter data, common and by type (current, voltage, power, energy by phase)
/// 
#[derive(Serialize, Deserialize, Debug)]
struct ElectricMeter {
    r_pwr_full: f32,
    u_energy_full: u64,
    o_currents: Current3Ph,
    o_voltages: Voltage3Ph,
}

fn main() {
    // Data as a constant
    let o_em = ElectricMeter { r_pwr_full : 25.45, u_energy_full: 24352345,
                               o_currents : Current3Ph { r_l1 : 11.3, r_l2: 12.3, r_l3: 9.4 },
                               o_voltages : Voltage3Ph {
                                   r_l1_n :  220.4, r_l2_n : 230.5, r_l3_n:  231.2,
                                   r_l1_l2 : 384.4, r_l2_l3: 387.1, r_l3_l1: 376.5,
                              }};
    // Serialize to a string
    let s_serialized = serde_json::to_string(&o_em).expect("Sorry, can't serialize your data!");
    // let s_serialized = serde_json::to_string(&o_em).unwrap();
    println!("Serialized output:\n{}", s_serialized);
    
    // just for testing, make some substitutions in a string to invalidate it as YAML input
    // let s_test :String = str::replace(&s_serialized, ",", ";");
    let s_test :String = s_serialized;
    

    // load from a string as YAML
    // let o_from_yaml :ElectricMeter = serde_yaml::from_str(&s_serialized).expect("Excuse me, I can't understand your YAML");
    let o_from_yaml :ElectricMeter = serde_yaml::from_str(&s_test).expect("Excuse me, I can't understand your YAML");
    println!("Deserialized data: {:?}", o_from_yaml)
}
