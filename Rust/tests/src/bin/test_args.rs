// import of external modules
use std::io::Write;
use std::str::FromStr;


/// Search for greatest common divisor (GCD) of two numbers.
/// 
/// # Parameters: 2 numbers (u64)
/// # Returns: GCD (u64)
/// # Exceptions: assert if at least one of arguments == 0
fn gcd(mut n: u64, mut m: u64) -> u64 {
    assert!(n != 0 && m != 0);
    while m != 0 {
        if m < n { let t = m ; m = n ; n = t };
        m = m % n;
    }
    n
}


/// Unit testing of gcd() function
/// 
#[test]
fn test_gcd() {
    assert_eq!(gcd(2 * 5 * 11 * 17,
                   3 * 7 * 13 * 19),
               1);
    assert_eq!(gcd(2 * 3 * 5 * 11 * 17,
                   3 * 7 * 11 * 13 * 19),
               3 * 11);
}


fn main() {
    let mut vm_args = Vec::new();

    // arguments processing
    for s_arg in std::env::args().skip(1) {
        vm_args.push(u64::from_str(&s_arg).expect("Error parsing argument"));
    }
    let v_args = vm_args;

    if v_args.len() == 0 {
        // no arguments given
        writeln!(std::io::stderr(), "Usage: test_args <NUMBER> <NUMBER> [NUMBER ...]").unwrap();
        std::process::exit(1);
    }

    // find greatest common divisor and save it in a variable
    let mut u_gcd = v_args[0];
    for m in &v_args[1..] { u_gcd = gcd(u_gcd, *m) };
    
    // print results
    println!("The greatest common divisor of {:?} is {}",
             v_args, u_gcd);
}
