#[macro_use]
extern crate libmodbus_rs;
extern crate bytes;
extern crate byteorder;

use bytes::{Bytes, BytesMut, Buf, BufMut, IntoBuf};
use byteorder::ByteOrder;
use byteorder::LittleEndian as LE;
use byteorder::BigEndian as BE;
use byteorder::NativeEndian as NE;
// what to use from Modbus module (traits)
use libmodbus_rs::{Modbus, ModbusRTU, ModbusTCP, ModbusClient};


const ADDRESS_START: u16 = 7;
const ADDRESS_END: u16 = 23;
const SERVER_IP :&'static str = "10.1.129.142";
const SERVER_PORT :i32 = 502;
const SLAVE_ID :u8 = 1;


enum Endianess { BE, LE, NE}
const E :Endianess = Endianess::LE;

fn buf_to_f32(src :BytesMut) -> f32 {
    // Initialize variables, create buffer
    assert_eq!(src.len(), 4);
    src.into_buf().get_f32::<BE>()
}


// swaps two 16-bit words in a 32-bit buffer
fn swap_16b_words(b: BytesMut) -> BytesMut {
    use std::mem::swap;
    let mut newbuf = b.clone();
    let mut temp = b.into_buf();
    let first = temp.get_u16::<NE>();
    let second = temp.get_u16::<NE>();
    newbuf.put_u16::<NE>(second);
    newbuf.put_u16::<NE>(first);
    println!("swap_16b_words: converted {:?} to {:?}", temp, newbuf);
    newbuf
}

// parameters: slice of i16 vector
// returns: tuple, 1st value is length of buffer in bytes, 2nd is buffer (BytesMut)
fn vecint_to_bytes(src: &[u16]) -> BytesMut {
    use std::mem::size_of;
    let mut mbuf = BytesMut::with_capacity((src.len() + 1) * size_of::<u16>());
    for i in src {
        mbuf.put_u16::<BE>(*i);
     }
    println!("vecint_to_bytes buffer:");
    for i in &mbuf {
        print!(" {:02x}",i);
    }
    println!("");
    mbuf
}


fn test_mb_tcp() {
    let mut modbus = Modbus::new_tcp(SERVER_IP, SERVER_PORT).expect("Cannot open network connection");
    modbus.set_slave(SLAVE_ID);
    modbus.set_debug(false).expect("could not set modbus DEBUG mode");
    modbus.connect().expect("could not connect");

    let num_words :u16 = ADDRESS_END - ADDRESS_START;
    // let mut request_registers = vec![0u16; num_words];
    let mut response_registers = vec![0u16; num_words as usize];
    // let mut rw_request_registers = vec![0u16; num_words];
    let mut num_failures = 0;
    match modbus.read_registers(ADDRESS_START as u16, num_words, &mut response_registers) {
        Err(err) => {
            println!("ERROR modbus_read_registers ({:?})", err);
            println!("Address = {}, num_words = {}", ADDRESS_START, num_words);
            num_failures += 1;
        },
        Ok(_len) => {
            println!("Success, {} words were read", _len);
            // print registers word-by-word
            for w in &response_registers {
                println!("--> {:04x}", w)
            }
            // process my data as f32
            let ai1 = buf_to_f32(vecint_to_bytes(&response_registers[0..2]));
            let ai2 = buf_to_f32(vecint_to_bytes(&response_registers[2..4]));
            let ai3 = buf_to_f32(vecint_to_bytes(&response_registers[4..6]));
            let ai4 = buf_to_f32(vecint_to_bytes(&response_registers[6..8]));
            let ai5 = buf_to_f32(vecint_to_bytes(&response_registers[8..10]));
            let ai6 = buf_to_f32(vecint_to_bytes(&response_registers[10..12]));
            let ai7 = buf_to_f32(vecint_to_bytes(&response_registers[12..14]));
            let ai8 = buf_to_f32(vecint_to_bytes(&response_registers[14..16]));
            println!("Analog inputs: ai1: {:4.5}, ai2: {:4.5}, ai3: {:4.5}, ai4: {:4.5}. ai5: {:4.5}", ai1, ai2, ai3, ai4, ai5);
        }
    }
    modbus.close();
}

fn main() {
    test_mb_tcp();
}

