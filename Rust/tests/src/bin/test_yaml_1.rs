#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_yaml;
extern crate yaml_rust;

use yaml_rust::yaml;
use yaml_rust::yaml::{Yaml, YamlLoader};


#[derive(Debug, Clone, Copy)]
enum ModBusDataTypes {
    i32,
    i64,
    f32,
    f64,
}

impl ModBusDataTypes {
    pub fn from_str(s: &str) -> Option<ModBusDataTypes> {
        match s {
            "i32" => Some(ModBusDataTypes::i32),
            "i64" => Some(ModBusDataTypes::i64),
            "f32" => Some(ModBusDataTypes::f32),
            "f64" => Some(ModBusDataTypes::f64),
            _ => { println!("ModBusDataType::from_str: Incorrect value {:?} to convert", s); None },
        }
    }
    
}

impl std::fmt::Display for ModBusDataTypes {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let myself_str = match *self {
            ModBusDataTypes::i32 => "i32" ,
            ModBusDataTypes::i64 => "i64" ,
            ModBusDataTypes::f32 => "f32" ,
            ModBusDataTypes::f64 => "f64" ,
        };
        write!(f, "{}", myself_str)
    }
}

fn main() {
    let _docs = "
        ElectricMeter: 
          phases: 3

          energy:
            type: i64
            Full: 3204
            Phase: [3518, 3522, 3526]

          power:
            type: f32
            Phase: [3054, 3056, 3058]
            Full: 3060

          current:
            type: f32
            Phase: [3000, 3002, 3004]
            Mean: 3010

          voltage:
            type: f32
            LN: [3028, 3030, 3032]
            LL: [3020, 3022, 3024]
        ";
    let doc2 = YamlLoader::load_from_str(_docs);
    let doc1 = YamlLoader::load_from_str(r"{f32: 3028, f64: 3036, i64:3709, i64:2343}");
    println!("Yaml documents: {:?}", doc2);
    match doc1 {
        Ok(docvec) => {
            // next cycle probably will run only once
            for doc in &docvec {
                println!("Doc1: {:?}", doc);
                match *doc {
                    // now we need to deconstruct a hash with pattern-matching
                    Yaml::Hash(ref h) => {
                        for (k, v) in h {
                            let k = ModBusDataTypes::from_str(k.as_str().unwrap());
                            match (k, v) {
                                (Some(ModBusDataTypes::f32), &Yaml::Integer(ref addr)) => {
                                    println!("addr-type Hash!, type float32, addr {}", addr);
                                },
                                (Some(ModBusDataTypes::f64), &Yaml::Integer(ref addr)) => {
                                    println!("addr-type Hash!, type float64, address {}", addr);
                                },
                                (Some(typ), &Yaml::Integer(ref addr)) => {
                                    println!("addr-type Hash!, type {0}, address {1}", typ, addr);
                                },
                                (None, _) => {panic!("Invalid datatype occured in Yaml hash {:?}", doc)},
                                _ => unreachable!(),
                            }
                        }
                    },
                    /* Yaml::Hash(_) => println!("Any other Hash!"), */
                    _ => unreachable!(),
                }
            }
        },
        _ => panic!("bad Yaml"),
    }
    // let _from_yaml :ElectricMeter = serde_yaml::from_str(&docs).expect("Excuse me, I can't understand your YAML");
}
