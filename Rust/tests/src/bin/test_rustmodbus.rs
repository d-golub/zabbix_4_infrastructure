/// Test program for communication with ModBus device 
/// 
/// Device: (Electric Meter iEM3155 via Moxa terminal server)
/// 

// external libraries
extern crate modbus;

use modbus::tcp;
use modbus::Client;
use std::time::Duration;


fn test_tcp() {
    // Constants
    let s_ip = "127.0.0.1";
    let i_port = 4004;
    let i_start = 3000;
    let i_reg_count = 12;
    let i_modbus_slaveid = 1;
    let t_tcp_timeout = Duration::from_secs(2);

    let o_tcp_cfg = tcp::Config {
        tcp_port: i_port,
        tcp_read_timeout: Some(t_tcp_timeout),
        tcp_write_timeout: None,
        modbus_uid : i_modbus_slaveid,
    };
    let mut client = tcp::Transport::new_with_cfg(s_ip, o_tcp_cfg).unwrap();
    println!("{:?}", client.read_holding_registers(i_start, i_reg_count).expect("IO Error"));
}


fn test_rtu() {
    println!("Modbus-RTU not implemented yet!");
    std::process::exit(1)
}

fn main() {
    test_tcp()
}
