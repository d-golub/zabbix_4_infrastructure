#[macro_use] extern crate failure;
#[macro_use] extern crate failure_derive;

use failure::Error;

// see https://github.com/withoutboats/failure/blob/master/failure-1.X/failure_derive/tests/tests.rs
#[derive(Fail, Debug)]
enum MyError {
    #[fail(display = "TestError1: {}", _0)]
    TestError1(&'static str),
    #[fail(display = "TestError2 occured: {}", _0)]
    TestError2(&'static str),
}

fn bailer() -> Result<(), Error> {
    println!("*TRACE* Bailer called");
    // bail!("bailed out of bailer function {}", "bailer");
    Ok(())
}

fn ensures() -> Result<(), Error> {
    println!("*TRACE* Ensures called");
    ensure!(true, "true is NOT false");
    // ensure!(false, "false is false");
    Ok(())
}

fn incorrect_parse() -> Result<(), Error> {
    println!("*TRACE* incorrect_parse called");
    if let Ok(_) = "0123123124".parse::<i32>() {
        return Ok(())
    }
    bail!("Incorrect string for parsing");
}

fn my_error() -> Result<(), failure::Error> {
    bail!(MyError::TestError1("A test error"))
    // bail!(MyError::TestError2("A test error"))
}

use std::fs::File;
use std::io::prelude::*;

fn read_file() -> Result<(), failure::Error> {
    println!("*TRACE* read_file called");
    let mut f_in = File::open("/tmp/test.file")?;
    let mut s_buf = String::new();
    f_in.read_to_string(&mut s_buf)?;
    // Ok(s_buf.trim().to_string())
    Ok(())
}

fn test1() {
    match bailer() {
        Ok(_) => println!("bailer() ok"),
        Err(e) => println!("Error received: {}", e),
    }
    match ensures() {
        Ok(_) => println!("ensures() ok"),
        Err(e) => println!("Error from 'ensures' function: {}", e),
    }
    match incorrect_parse() {
        Ok(_) => println!("Parsed an integer value"),
        Err(e) => println!("Error from 'parse': {}", e),
    }
    match read_file() {
        Ok(_) => println!("File contains string"),
        Err(e) => println!("Error {} when reading file", e),
    }
    match my_error() {
        Ok(_) => println!("Strange, there is no 'ok' variant here"),
        Err(e) => println!("Error {} in function my_error", e),
    }
}

fn test2() -> Result<(), Error> {
    bailer()
    .and_then(|_| ensures())
    .and_then(|_| incorrect_parse())
    .and_then(|_| read_file())
    .and_then(|_| my_error())
}

fn test3() -> Result<(), Error> {
    bailer()?;
    ensures()?;
    incorrect_parse()?;
    read_file()?;
    my_error()?;
    Ok(())
}

fn main() {
    println!("\n\n==== Test 1 ====\n");
    test1();
    println!("\n\n==== Test 2 ====\n");
    match test2() {
        Ok(_) => {},
        Err(e) => println!("Test 2 failed: {}", e),
    }
    println!("\n\n==== Test 3 ====\n");
    match test3() {
        Ok(_) => {},
        Err(e) => println!("Test 3 failed miserably: {}", e),
    }
}
