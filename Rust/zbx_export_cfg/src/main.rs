#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate restson;
use restson::{RestClient,RestPath,Error};

#[derive(Serialize, Deserialize, Debug)]
struct ZbxError {
    code: i32,
    data: String,
    message: String,
}


// #[derive(Serialize, Deserialize, Debug)]
// enum ZbxResultOrError {
//     result: String,
//     error: ZbxError,
// }

#[derive(Serialize, Deserialize, Debug)]
enum ZbxResponse {
    Ok{ jsonrpc: String, id: u32, result: String },
    Err{ jsonrpc: String, id: u32, error: ZbxError },
}

#[derive(Serialize, Deserialize, Debug)]
struct RequestToZabbix {
    id: u32,
    jsonrpc: f32,
    method: String,
    params: Vec<String>,
}
// params: RequestParams,

impl RestPath<RequestToZabbix> for RequestToZabbix {
    fn get_path(_:RequestToZabbix) -> Result<String,Error> {
        Ok("/zabbix/api_jsonrpc.php".to_string())
    }
}

impl RestPath<()> for RequestToZabbix {
    fn get_path(_:()) -> Result<String,Error> {
        Ok("/zabbix/api_jsonrpc.php".to_string())
    }
}


fn do_work() {
    let mut client = RestClient::new("http://infrazab.hostco.ru").unwrap();
    let vers_req = RequestToZabbix{id: 1, jsonrpc: 2.0, method: "apiinfo.version".to_string(), params: vec![]};
    let zbx_resp : ZbxResponse;
    let params = vec![];
    let res: serde_json::Value = client.post_capture_with((), &vers_req, &params).unwrap();
    println!("{:?}", res);
    // What did we receive? Is it OK or Error ?  'res' is an object of type serde_json::Value.
    if res.is_object() {
        // XXX see https://www.reddit.com/r/rust/comments/7hasv6/mixed_valuestruct_deserialization_with_serde_json/
        match res.get("error") {
            Some(err) => {
                println!("Error: {}!", err);
                let zbx_resp: ZbxResponse::Err = serde_json::from_str(res);
            }
            None => {
                let zbx_resp: ZbxResponse::Ok = serde_json::from_str(res);
            }
        }
    }
}

fn main() {
    do_work();
    println!("End of program run");
}
