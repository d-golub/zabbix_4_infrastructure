-- *****************************************************************
-- Moxa Active Ethernet I/O Server MIB
--
-- 01-03-2013
--
-- Copyright (c) 2013 by Moxa Automation Co., Ltd.
-- All rights reserved.
-- *****************************************************************

MOXA-IO-E1260-MIB DEFINITIONS ::= BEGIN
    IMPORTS
        enterprises, Unsigned32, Integer32, MODULE-IDENTITY, OBJECT-TYPE FROM SNMPv2-SMI;
   
-- 1.3.6.1.4.1.8691.10.1260
    e1260   MODULE-IDENTITY
    LAST-UPDATED "201302211400Z"
    ORGANIZATION "Moxa Automation,Inc."
    CONTACT-INFO
            "Postal: Moxa Automation,Inc.
             Fl.4, No.135,Lane 235,Pao-Chiao Rd.
             Shing Tien City,Taipei,Taiwan,R.O.C
             Tel: +866-2-89191230 "
    DESCRIPTION
            "The MIB module for Moxa ioLogik Remote Ethernet I/O specific information." 
    REVISION "201302211400Z"
    DESCRIPTION
            "First version of this MIB."
           ::= { ioLogik 1260 }    -- 1.3.6.1.4.1.8691.10.1260

-- 1.3.6.1.4.1.8691
    moxa OBJECT IDENTIFIER ::= { enterprises 8691 }

-- 1.3.6.1.4.1.8691.10
    ioLogik OBJECT IDENTIFIER ::= { moxa 10 }

-- 1.3.6.1.4.1.8691.10.1260.1
    totalChannelNumber OBJECT-TYPE
        SYNTAX Integer32 (1..16)
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "Total I/O channels."
        ::= { e1260 1 }

-- 1.3.6.1.4.1.8691.10.1260.2
    serverModel OBJECT-TYPE
        SYNTAX OCTET STRING
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "The I/O server model."
        ::= { e1260 2 }

-- 1.3.6.1.4.1.8691.10.1260.3
    systemTime OBJECT-TYPE
        SYNTAX Integer32
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "I/O server up time (in seconds)."
        ::= { e1260 3 }

-- 1.3.6.1.4.1.8691.10.1260.4
    firmwareVersion OBJECT-TYPE
        SYNTAX OCTET STRING
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "The firmware version."
        ::= { e1260 4 }


----------------------------------------------------------------
-- I/O
-- 1.3.6.1.4.1.8691.10.1260.10
	e1260monitor OBJECT IDENTIFIER ::= { e1260 10 }

----------------------------------------------------------------
-- RTD
rtdTable OBJECT-TYPE
		SYNTAX 			SEQUENCE OF RTDEntry
		MAX-ACCESS 		not-accessible
		STATUS 			current
		DESCRIPTION		"The RTD channel monitor table"
		::= { e1260monitor 6 }

rtdEntry OBJECT-TYPE
		SYNTAX			RTDEntry
		MAX-ACCESS 		not-accessible
		STATUS 			current
		DESCRIPTION		"The RTD channel monitor item"
		INDEX { rtdIndex }
		::= { rtdTable 1 }

RTDEntry ::=	SEQUENCE {
		rtdIndex		Integer32,
		rtdEnable		Integer32,
		rtdType			Integer32,
		rtdUnitType		Integer32,
		rtdValue		Integer32
}

rtdIndex OBJECT-TYPE
		SYNTAX 			Integer32 (0..5)
		MAX-ACCESS 		read-only
		STATUS 			current
		DESCRIPTION		"The RTD channel index."
		::= { rtdEntry 1 }    

rtdEnable OBJECT-TYPE
		SYNTAX 			Integer32 (0..1)
		MAX-ACCESS 		read-write
		STATUS 			current
		DESCRIPTION		"The RTD channel Enable/Disable. Disable=0, Enable=1"
		::= { rtdEntry 2 }
      
rtdType OBJECT-TYPE
		SYNTAX 			Integer32 (0..17)
		MAX-ACCESS 		read-write
		STATUS 			current
		DESCRIPTION		"The RTD channel type. PT50=0, PT100=1,PT200=2,PT500=3,PT1000=4,RES1-310=14,RES1-620=15,RES1-1250=16,RES1-2200=17"
		::= { rtdEntry 3 }
		   
rtdUnitType OBJECT-TYPE
		SYNTAX 			Integer32 (0..2)
		MAX-ACCESS 		read-write
		STATUS 			current
		DESCRIPTION		"The RTD channel range. CELSIUS=0, FAHRENHEIT=1, OHM=2"
		::= { rtdEntry 4 }
		
rtdValue OBJECT-TYPE
		SYNTAX 			Integer32 (0..4294967295)
		MAX-ACCESS 		read-only
		STATUS 			current
		DESCRIPTION		"The RTD channel value."
		::= { rtdEntry 5 }


                         

----------------------------------------------------------------
-- RTD TRAP Greater 
-- 1.3.6.1.4.1.8691.10.1260.26
    rtdTrap_Greater OBJECT IDENTIFIER ::= { e1260 26 }

----------------------------------------------------------------
rtdTrapG0 TRAP-TYPE
	ENTERPRISE 		rtdTrap_Greater 
	DESCRIPTION 		"The RTD channel-00 Greater Trap."
	::= 1

rtdTrapG1 TRAP-TYPE
	ENTERPRISE 		rtdTrap_Greater 
	DESCRIPTION 		"The RTD channel-01 Greater Trap."
	::= 2

rtdTrapG2 TRAP-TYPE
	ENTERPRISE 		rtdTrap_Greater 
	DESCRIPTION 		"The RTD channel-02 Greater Trap."
	::= 3

rtdTrapG3 TRAP-TYPE       
	ENTERPRISE 		rtdTrap_Greater 
	DESCRIPTION 		"The RTD channel-03 Greater Trap."
	::= 4

rtdTrapG4 TRAP-TYPE       
	ENTERPRISE 		rtdTrap_Greater 
	DESCRIPTION 		"The RTD channel-04 Greater Trap."
	::= 5    
	
rtdTrapG5 TRAP-TYPE       
	ENTERPRISE 		rtdTrap_Greater 
	DESCRIPTION 		"The RTD channel-05 Greater Trap."
	::= 6


----------------------------------------------------------------
-- RTD TRAP Greater 
-- 1.3.6.1.4.1.8691.10.1260.27
    rtdTrap_Smaller OBJECT IDENTIFIER ::= { e1260 27 }

----------------------------------------------------------------
rtdTrapS0 TRAP-TYPE
	ENTERPRISE 		rtdTrap_Smaller 
	DESCRIPTION 		"The RTD channel-00 Smaller Trap."
	::= 1

rtdTrapS1 TRAP-TYPE
	ENTERPRISE 		rtdTrap_Smaller 
	DESCRIPTION 		"The RTD channel-01 Smaller Trap."
	::= 2

rtdTrapS2 TRAP-TYPE
	ENTERPRISE 		rtdTrap_Smaller 
	DESCRIPTION 		"The RTD channel-02 Smaller Trap."
	::= 3

rtdTrapS3 TRAP-TYPE       
	ENTERPRISE 		rtdTrap_Smaller 
	DESCRIPTION 		"The RTD channel-03 Smaller Trap."
	::= 4

rtdTrapS4 TRAP-TYPE       
	ENTERPRISE 		rtdTrap_Smaller 
	DESCRIPTION 		"The RTD channel-04 Smaller Trap."
	::= 5    
	
rtdTrapS5 TRAP-TYPE       
	ENTERPRISE 		rtdTrap_Smaller 
	DESCRIPTION 		"The RTD channel-05 Smaller Trap."
	::= 6

----------------------------------------------------------------
-- Message TRAP
-- 1.3.6.1.4.1.8691.10.1260.30
	messageTrap OBJECT IDENTIFIER ::= { e1260 30 }

----------------------------------------------------------------
activeMessageTrap TRAP-TYPE
	ENTERPRISE 		messageTrap
	DESCRIPTION 	"The SNMP trap with active message"
	::= 1

END

