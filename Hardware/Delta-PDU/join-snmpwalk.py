#!/usr/bin/env python3
import sys

"""
Слияние двух файлов с выводом SNMPWALK (при подключенном MIB-е), при этом из
соответствующих строк каждого файла формируется одна строка выходного файла вот
так:

Файл 1>> DeltaUPS-MIB::dupsIdentManufacturer.0 = STRING: "Delta"
Файл 2>> .1.3.6.1.4.1.2254.2.4.1.1.0 = STRING: "Delta"

Результат>> 
 DeltaUPS-MIB::dupsIdentManufacturer.0 (.1.3.6.1.4.1.2254.2.4.1.1.0) = STRING: "Delta"
"""

with open(sys.argv[1], "r") as fSym:
    with open(sys.argv[2], "r") as fNum:
        it_sym = iter(fSym)
        it_num = iter(fNum)
        try:
            while True:
                (s_name, s_sym_val) = next(it_sym).strip().split(" = ")
                (s_oid, s_oid_val) = next(it_num).strip().split(" = ")
                if s_sym_val == s_oid_val:
                    print("{0} \t({2}) = \t{1}".format(s_name, s_sym_val, s_oid))
                else:
                    print("!!! Name {0} and OID {2} ({1} != {3})".format(
                          s_name, s_sym_val, s_oid, s_oid_val))
        except StopIteration:
            pass
