---
title: Zabbix Appliance
---

## Настройка временной зоны

Кроме общесистемной настройки времени (`/etc/timezone` и `/etc/localtime`), нужно выставить
временнУю зону для Zabbix файле `/etc/apache2/conf-available/zabbix.conf`, в секциях `mod_php5.c` и `mod_php7.c`.
По умолчанию там была Рига (UTC+2 зимой).

````
php_value date.timezone Asia/Yekaterinburg
````

После этого надо перестартовать сервис `apache2`.
