---
title: Тестирование Modbus модуля для Zabbix

---

# Цель

Существует [Modbus модуль для
Zabbix](https://github.com/v-zhuravlev/libzbxmodbus), работающий на базе
библиотеки "[libmodbus](https://github.com/stephane/libmodbus)". Есть смысл его
попробовать, возможно, это сэкономит усилия по разработке ПО.

# Ход работы

## Компиляция

Собирал модуль на своей машине, командная строка для `configure`:

```
./configure --prefix=/etc/zabbix --enable-zabbix-3.2
```

Для 3.4 ещё не сделали опции, но есть надежда, что будет работать.

## Инсталляция

Перенёс библиотеки из каталога `src/.libs` после сборки на машину `infrazab.hostco.ru`
во вновь созданный каталог `/usr/lib/zabbix/modules`:

```
appliance@infrazab:/usr/lib/zabbix/modules$ ls -l 
total 196
lrwxrwxrwx 1 root root     18 Feb  7 14:29 libmodbus.so -> libmodbus.so.5.1.0
lrwxrwxrwx 1 root root     18 Feb  7 14:29 libmodbus.so.5 -> libmodbus.so.5.1.0
-rwxr-xr-x 1 root root 159824 Feb  7 14:29 libmodbus.so.5.1.0
-rwxr-xr-x 1 root root  34872 Feb  7 14:28 libzbxmodbus.so
```

Проверил, что есть необходимые для `libzbxmodbus.so` библиотеки:

```
appliance@infrazab:/usr/lib/zabbix/modules$ ldd libzbxmodbus.so 
        linux-vdso.so.1 =>  (0x00007ffc3c5a1000)
        libmodbus.so.5 => /usr/lib/x86_64-linux-gnu/libmodbus.so.5 (0x00007fd76050b000)
        libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007fd760141000)
        /lib64/ld-linux-x86-64.so.2 (0x00007fd760919000)
```

Подхватилась системная библиотека `libmodbus`, ничего страшного.

UPD: с системной библиотекой `libmodbus` модуль (после правки конфигурации
Zabbix, [см. ниже](cfg_zbx)) отказался работать, пришлось удалить пакет `libmodbus5` и
добавить файл `/etc/ld.so.conf.d/01_zabbix_modules.conf` с путём к каталогу
модулей Zabbix: `/usr/lib/zabbix/modules`.

После удаления пакета `libmodbus5` и перезагрузки ld находит уже новую, поставленную вместе
с модулем библиотеку `libmodbus`:

```
appliance@infrazab:/usr/lib/zabbix/modules$ ldd libzbxmodbus.so                                                                                linux-vdso.so.1 =>  (0x00007ffeb81ee000)
    libmodbus.so.5 => /usr/lib/zabbix/modules/libmodbus.so.5 (0x00007fd55b038000)
    libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007fd55ac6e000)
    /lib64/ld-linux-x86-64.so.2 (0x00007fd55b448000)
```

И сервер Zabbix стартует нормально.

## Настройка Zabbix {cfg_zbx}

В конфигурационном файле `/etc/zabbix/zabbix_server.conf` в секции «Loadable modules» написал:

```
### Option: LoadModulePath
LoadModulePath=${libdir}/modules
...

### Option: LoadModule
LoadModule=libzbxmodbus.so
```

В соответствии с рекомендациями автора модуля увеличил таймаут до 10 секунд (параметр `Timeout`).

## Тестирование

После того, как добился старта Zabbix, проверяю, что опрос оборудования происходит нормально,
с помощью `zabbix_get`:

Пытаюсь получить значение через Incapsulated RTU шлюз:

```
%> /opt/ModPoll/bin/modpoll -m enc -r 3000 -f -t 4:float -p 4004 -1 10.1.129.140                                            >>>>>>>>>-
modpoll 3.4 - FieldTalk(tm) Modbus(R) Master Simulator
Copyright (c) 2002-2013 proconX Pty Ltd
Visit http://www.modbusdriver.com for Modbus libraries and tools.

Protocol configuration: Encapsulated RTU over TCP
Slave configuration...: address = 1, start reference = 3000, count = 1
Communication.........: 10.1.129.140, port 4004, t/o 1.00 s, poll rate 1000 ms
Data type.............: 32-bit float, output (holding) register table
Word swapping.........: Slave configured as big-endian float machine

-- Polling slave...
[3000]: 31.599401

appliance@infrazab:/usr/lib/zabbix/modules$ zabbix_get -s 127.0.0.1 -k 'modbus_read[enc://10.1.129.140,1,3000,4,f,1]'
ZBX_NOTSUPPORTED: Unsupported item key.
```

Аналогично с Моксы (пытаюсь считать датчик температуры):

```
%> /opt/ModPoll/bin/modpoll -m tcp -r 11  -t 3:float -1 10.1.129.142                                                        >>>>>>>>>-
modpoll 3.4 - FieldTalk(tm) Modbus(R) Master Simulator
Copyright (c) 2002-2013 proconX Pty Ltd
Visit http://www.modbusdriver.com for Modbus libraries and tools.

Protocol configuration: MODBUS/TCP
Slave configuration...: address = 1, start reference = 11, count = 1
Communication.........: 10.1.129.142, port 502, t/o 1.00 s, poll rate 1000 ms
Data type.............: 32-bit float, input register table

-- Polling slave...
[11]: 21.695276


appliance@infrazab:/usr/lib/zabbix/modules$ zabbix_get -s 127.0.0.1 -k 'modbus_read[enc://10.1.129.142,1,11,3,f,1]'
ZBX_NOTSUPPORTED: Unsupported item key.
```

Возможно, нужно создать соотвествующий айтем в Заббиксе? Проверяю.  Создал хост
и в нём пару айтемов (поля с пустымы значениями удалены):

```{xml}
<host>
    <host>Test_Moxa_ModBus</host>
    <name>Тестовый Modbus (через модуль) к Moxa 1240</name>
    <description>Для тестирования Modbus модуля</description>
    <status>0</status>
    <ipmi_authtype>-1</ipmi_authtype>
    <ipmi_privilege>2</ipmi_privilege>
    <tls_connect>1</tls_connect>
    <tls_accept>1</tls_accept>
    <groups>
	<group>
	    <name>Moxa</name>
	</group>
    </groups>
    <interfaces>
	<interface>
	    <default>1</default>
	    <type>1</type>
	    <useip>1</useip>
	    <ip>127.0.0.1</ip>
	    <port>10050</port>
	    <bulk>1</bulk>
	    <interface_ref>if1</interface_ref>
	</interface>
    </interfaces>
    <applications/>
    <items>
	<item>
	    <name>Humidity</name>
	    <type>3</type>
	    <key>modbus_read[tcp://10.1.129.142,1,9,3,f,0,0]</key>
	    <delay>10s</delay>
	    <history>1d</history>
	    <trends>10d</trends>
	    <status>0</status>
	    <value_type>0</value_type>
	    <allowed_hosts/>
	    <units>%</units>
	    <description>Влажность через modbus</description>
	    <interface_ref>if1</interface_ref>
	</item>
	<item>
	    <name>Temperature</name>
	    <type>3</type>
	    <key>modbus_read[tcp://10.1.129.142,1,11,3,f,0,0]</key>
	    <delay>10s</delay>
	    <history>1d</history>
	    <trends>10d</trends>
	    <status>0</status>
	    <value_type>0</value_type>
	    <units>°C</units>
	    <description>Температура через modbus</description>
	    <inventory_link>0</inventory_link>
	    <interface_ref>if1</interface_ref>
	</item>
    </items>
</host>
```

Ничего хорошего не получилось: айтемы перешли в состояние Unsupported, `modbus_read` по-прежнему возвращает `Unsupported item key`.

## Тестирование на эмуляторе ModBus устройства.

Запустил на своём ноутбуке эмулятор ModBus сервера, чтобы потестировать на нём.  Java-вский эмулятор (ModbusPal) не удалось
завести нормально, запустил [diagslave](http://www.modbusdriver.com/diagslave.html):

```
%> ./linux/diagslave -m tcp -a 1 -p 8602
diagslave 2.12 - FieldTalk(tm) Modbus(R) Diagnostic Slave Simulator
Copyright (c) 2002-2012 proconX Pty Ltd
Visit http://www.modbusdriver.com for Modbus libraries and tools.

Protocol configuration: MODBUS/TCP
Slave configuration: address = 1, master activity t/o = 3.00
TCP configuration: port = 8602, connection t/o = 60.00

Server started up successfully.
Listening to network (Ctrl-C to stop)
....
validateMasterIpAddr: accepting connection from 10.5.10.10
Slave   1: readHoldingRegisters from 9, 5 references
```

Диагностика на консоль, любой запрос регистра возвращает 0.  Например:

```
%> /opt/ModPoll/bin/modpoll -m tcp -p 8602 -r 4 -0 -t 4 -c 2 -1 10.5.10.10                                                  >>>>>>>>>-
modpoll 3.4 - FieldTalk(tm) Modbus(R) Master Simulator
Copyright (c) 2002-2013 proconX Pty Ltd
Visit http://www.modbusdriver.com for Modbus libraries and tools.

Protocol configuration: MODBUS/TCP
Slave configuration...: address = 1, start reference = 4 (PDU), count = 2
Communication.........: 10.5.10.10, port 8602, t/o 1.00 s, poll rate 1000 ms
Data type.............: 16-bit register, output (holding) register table

-- Polling slave...
[4]: 0
[5]: 0

```
Ключ item-а в Zabbix: 	`modbus_read[tcp://10.5.10.10:8602,1,4,4,s,0,0]`
На консоли эмулятора в это время:

```
........
validateMasterIpAddr: accepting connection from 10.1.96.148
Slave   1: readInputRegisters from 5, 1 references
......
```

Увидел на консоли соединения с Zabbix, в соответствующий Item Заббикса записываются нули.
То есть эмулятор как-то работает.

## Возврат к тестированию на Moxa 1240

Удалось заставить работать ModBus/TCP доступ к Моксе, введя следующие ключи:

Влажность (линия AI-1): `modbus_read[tcp://10.1.129.142,1,8,4,f,0,0]`
Температура (AI-2): `modbus_read[tcp://10.1.129.142,1,10,4,f,0,0]`
