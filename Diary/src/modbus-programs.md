# ПО для работы с ModBus

## Утилита modpool

Программа `modpool` — бесплатная (но не FOSS) утилита. Работает хорошо, в частности, позволяет работать
с инкапсулированными данными Modbus-RTU в TCP. Буду использовать как Reference Implemenatation.

## qmodbus

Довольно примитивная программа, не работает с инкапсулированным трафиком, но можно посмотреть данные
на входе и выходе в 16-ричном виде. Может быть полезна при программировании устройств, но для задачи
чтения данных полезность ограничена.

## Python интерфейс

Из AUR не ставится, ставлю с помощью PIP.

```
%> sudo pip3 install pymodbus                                                                              >>>>>>>>>>
[sudo] password for dgolub: 
Sorry, try again.
[sudo] password for dgolub: 
Collecting pymodbus
  Downloading pymodbus-1.3.2-py2.py3-none-any.whl (77kB)
    100% |████████████████████████████████| 81kB 471kB/s 
Collecting twisted>=12.2.0 (from pymodbus)
  Downloading Twisted-17.9.0.tar.bz2 (3.0MB)
    100% |████████████████████████████████| 3.0MB 343kB/s 
Collecting pyserial>=2.6 (from pymodbus)
  Downloading pyserial-3.4-py2.py3-none-any.whl (193kB)
    100% |████████████████████████████████| 194kB 3.5MB/s 
Collecting zope.interface>=4.0.2 (from twisted>=12.2.0->pymodbus)
  Downloading zope.interface-4.4.3-cp36-cp36m-manylinux1_x86_64.whl (173kB)
    100% |████████████████████████████████| 174kB 4.2MB/s 
Collecting constantly>=15.1 (from twisted>=12.2.0->pymodbus)
  Downloading constantly-15.1.0-py2.py3-none-any.whl
Collecting incremental>=16.10.1 (from twisted>=12.2.0->pymodbus)
  Downloading incremental-17.5.0-py2.py3-none-any.whl
Collecting Automat>=0.3.0 (from twisted>=12.2.0->pymodbus)
  Downloading Automat-0.6.0-py2.py3-none-any.whl
Collecting hyperlink>=17.1.1 (from twisted>=12.2.0->pymodbus)
  Downloading hyperlink-17.3.1-py2.py3-none-any.whl (73kB)
    100% |████████████████████████████████| 81kB 6.1MB/s 
Requirement already satisfied: setuptools in /usr/lib/python3.6/site-packages (from zope.interface>=4.0.2->twisted>=12.2.0->pymodbus)
Requirement already satisfied: six in /usr/lib/python3.6/site-packages (from Automat>=0.3.0->twisted>=12.2.0->pymodbus)
Requirement already satisfied: attrs in /usr/lib/python3.6/site-packages (from Automat>=0.3.0->twisted>=12.2.0->pymodbus)
Installing collected packages: zope.interface, constantly, incremental, Automat, hyperlink, twisted, pyserial, pymodbus
  Running setup.py install for twisted ... done
Successfully installed Automat-0.6.0 constantly-15.1.0 hyperlink-17.3.1 incremental-17.5.0 pymodbus-1.3.2 pyserial-3.4 twisted-17.9.0 zope.interface-4.4.3
```

Исходные тексты библиотеки pymodbus [на GitHub](https://github.com/riptideio/pymodbus/).


