# Мониторинг электрического счётчика, подключенного через терминальный сервер Moxa

Сервер — Moxa NPort_6450, находится на статическом IP адресе 10.1.129.140. Сервер
4-портовый, на 4 порту подключено по RS485 интересующее нас устройство: электрический счётчик.
Точную модель счётчика узнаем чуть ниже, но он принадлежит к семейству Schneider Electric iEM3x00.

Мокса сконфигурирована в режиме Socket, протокол Modbus-RTU инкапсулируется в TCP пакеты
(не используется Modbus-TCP)

Из веб-интерфейса Моксы (или из выдачи `nmap` :-) ) получаем, что 4 последовательному порту
соответствует TCP порт 4004. Догадываюсь, что идентификатор счётчика на шине — единица.

Вооружился утилитой `modpoll` и задал следующую командную строку:

```
%> /opt/ModPoll/bin/modpoll -m enc -p 4004  -a 1 -t 4 -r 2016 -c 1 10.1.129.140
modpoll 3.4 - FieldTalk(tm) Modbus(R) Master Simulator
Copyright (c) 2002-2013 proconX Pty Ltd
Visit http://www.modbusdriver.com for Modbus libraries and tools.

Protocol configuration: Encapsulated RTU over TCP
Slave configuration...: address = 1, start reference = 2016, count = 1
Communication.........: 10.1.129.140, port 4004, t/o 1.00 s, poll rate 1000 ms
Data type.............: 16-bit register, output (holding) register table

-- Polling slave... (Ctrl-C to stop)
[2016]: 11
-- Polling slave... (Ctrl-C to stop)
[2016]: 11
-- Polling slave... (Ctrl-C to stop)
[2016]: 11
-- Polling slave... (Ctrl-C to stop)
[2016]: 11
-- Polling slave... (Ctrl-C to stop)
[2016]: 11
-- Polling slave... (Ctrl-C to stop)
[2016]: 11
-- Polling slave... (Ctrl-C to stop)
^C

```

Кстати, если попытаться спросить тот же регистр по протоколу Modbus-TCP (`-m tcp`), 
получаем таймауты на ответы:

```
...
-- Polling slave... (Ctrl-C to stop)
Reply time-out!
-- Polling slave... (Ctrl-C to stop)
Reply time-out!
...
```

Из документации на счётчик -- регистр (1w) 2016 соответствует типу питания, на
который подключен счётчик, в данном случае значение 11 -- это «3PH4W (3 фазы 4
провода)».

Спрошу что-либо более дельное, например, ток по 1 фазе (float32 по адресу 3000).

```
%> /opt/ModPoll/bin/modpoll -m enc -p 4004  -a 1 -t 4:float -r 3000 -c 1 10.1.129.140
modpoll 3.4 - FieldTalk(tm) Modbus(R) Master Simulator
Copyright (c) 2002-2013 proconX Pty Ltd
Visit http://www.modbusdriver.com for Modbus libraries and tools.

Protocol configuration: Encapsulated RTU over TCP
Slave configuration...: address = 1, start reference = 3000, count = 1
Communication.........: 10.1.129.140, port 4004, t/o 1.00 s, poll rate 1000 ms
Data type.............: 32-bit float, output (holding) register table

-- Polling slave... (Ctrl-C to stop)
[3000]: -7535754474038879059968.000000
-- Polling slave... (Ctrl-C to stop)
[3000]: 117.628555
-- Polling slave... (Ctrl-C to stop)
[3000]: -0.000000
-- Polling slave... (Ctrl-C to stop)
[3000]: 603663223488512.000000
-- Polling slave... (Ctrl-C to stop)
[3000]: 0.000000
-- Polling slave... (Ctrl-C to stop)
[3000]: 0.000000
-- Polling slave... (Ctrl-C to stop)
[3000]: -7.726785
-- Polling slave... (Ctrl-C to stop)
[3000]: -0.000000
-- Polling slave... (Ctrl-C to stop)
[3000]: 35089722374434003807844950708584448.000000
```

Получается ерунда, и понятно почему: порядок байтов рабочего ноута не совпадает
с тем, что выдаёт счётчик. К счастью, есть опция `-f`, которая переключает
low-endian и big-endian.

```
%> /opt/ModPoll/bin/modpoll -m enc -p 4004  -a 1 -t 4:float -f -r 3000 -c 1 10.1.129.140
modpoll 3.4 - FieldTalk(tm) Modbus(R) Master Simulator
Copyright (c) 2002-2013 proconX Pty Ltd
Visit http://www.modbusdriver.com for Modbus libraries and tools.

Protocol configuration: Encapsulated RTU over TCP
Slave configuration...: address = 1, start reference = 3000, count = 1
Communication.........: 10.1.129.140, port 4004, t/o 1.00 s, poll rate 1000 ms
Data type.............: 32-bit float, output (holding) register table
Word swapping.........: Slave configured as big-endian float machine

-- Polling slave... (Ctrl-C to stop)
[3000]: 25.840549
-- Polling slave... (Ctrl-C to stop)
[3000]: 25.539562
-- Polling slave... (Ctrl-C to stop)
[3000]: 25.599960
-- Polling slave... (Ctrl-C to stop)
[3000]: 25.639179
-- Polling slave... (Ctrl-C to stop)
^C
```


Эти цифры куда более осмысленные.  Попробовал и другие параметры из документации счётчика.
Запросил его название и точную модель. Каждое из этих значений -- это строка из 20 слов, начинающаяся
соответственно с адресов 30 и 50. Запрашиваю 20 значений типа 4:hex:

```
> /opt/ModPoll/bin/modpoll -m enc -p 4004  -a 1 -t 4:hex -f -r 30 -c 20 -1 10.1.129.140
...
```

Вернулась строка: 

0x456E 0x6572 0x6779 0x204D 0x6574 0x6572 0x2020 0x2020 0x2020 0x2020 0x2020
0x2020 0x2020 0x2020 0x2020 0x2020 0x2020 0x2020 0x2020 0x2020

Кодировка однобайтовая. начиная с 7 слова, идут пробелы. Первые 14 (по таблице Unicode):
«Energy Meter». Модель: «iEM3155».

