# Контроллеры Moxa

Используются три контроллера Moxa ioLogik, модели E1210, E1240 и E1260. Встаёт вопрос, как их найти
и отличить один от другого?

## Различение контроллеров по SNMP

В SNMP OID-ах фирме Moxa Automation, Inc отведена веточка `iso.3.6.1.4.1.8691`. Для устройств ioLogik 
используется ветка `.10`, далее нужно знать модель устройства.

```
%> snmpget -v 2c -Cf -c public 10.1.129.142  iso.3.6.1.4.1.8691.10.1240.1.0
SNMPv2-SMI::enterprises.8691.10.1240.1.0 = INTEGER: 16

%> snmpget -v 2c -Cf -c public 10.1.129.142  iso.3.6.1.4.1.8691.10.1240.2.0
SNMPv2-SMI::enterprises.8691.10.1240.2.0 = STRING: "E1240"

%> snmpget -v 2c -Cf -c public 10.1.129.141  iso.3.6.1.4.1.8691.10.1240.2.0
SNMPv2-SMI::enterprises.8691.10.1240.2.0 = No Such Object available on this agent at this OID

%> snmpget -v 2c -Cf -c public 10.1.129.141  iso.3.6.1.4.1.8691.10.1210.2.0
SNMPv2-SMI::enterprises.8691.10.1210.2.0 = STRING: "E1210"

%> snmpget -v 2c -Cf -c public 10.1.129.143  iso.3.6.1.4.1.8691.10.1260.2.0
SNMPv2-SMI::enterprises.8691.10.1260.2.0 = STRING: "E1260"
```

Таким образом, на адресе .141 находится E1210, на .142 — E1240, и на .143 — E1260.

Проверить наличие устройства ioLogik определённой модели можно, запросив 
количество каналов (целое, >=1).  OID будет `iso.3.6.1.4.1.8691.10.`__model__`.1.0`.

Заглянув в Заббикс, вижу, что есть стандартный OID для идентификации устройства:
`1.3.6.1.2.1.1.2.0` (__system.objectid__).  Значение этого параметра, напирмер, для
E1240: `SNMPv2-SMI::enterprises.8691.10.1210`

## Создание «хостов» в Zabbix

Для создания хостов необходимо, чтобы Discovery их нашёл, а потом привязанное к событию
«Discovery» действие (Action) создало хост, добавило его в нужную группу и привязало
к шаблону хоста (Template).

Создал связанные темплейты: общий для всех Moxa устройств, и темплейт для конкретного девайса.
Все порты нужно прописывать в device-specific темплейт.

Темплейты:

 Template for Moxa ioLogik devices:
 	общий

 Template for Moxa ioLogik E1210 (16*Digital IO):
 	Для E1210
 Template for Moxa ioLogik E1240 (8*Analog IO):
 	Для E1240
 Template for Moxa ioLogik E1260 (8*Analog IO):
 	Для E1260

## Значения параметров

### Устройство E1240

Нужно внимательно читать (глазами) MIB базы. В частности, в файле `moxa_io_e1240_v1_0_build13022114.mib`
описана структура `aiEntry`, которая описывает аналоговую линию:

```
AIEntry ::= SEQUENCE {
                aiIndex         Integer32,
                aiEnable        Integer32,
                aiMode          Integer32,
                aiValue         Integer32,
                aiMin           Integer32,
                aiMax           Integer32
	    }
```
В выводе `snmpwalk` для двух подключенных у нас аналоговых линий это выглядит так:

```
iso.3.6.1.4.1.8691.10.1240.10.4.1.1.1 = INTEGER: 0
iso.3.6.1.4.1.8691.10.1240.10.4.1.1.2 = INTEGER: 1
...
iso.3.6.1.4.1.8691.10.1240.10.4.1.2.1 = INTEGER: 1
iso.3.6.1.4.1.8691.10.1240.10.4.1.2.2 = INTEGER: 1
...
iso.3.6.1.4.1.8691.10.1240.10.4.1.3.1 = INTEGER: 1
iso.3.6.1.4.1.8691.10.1240.10.4.1.3.2 = INTEGER: 1
...
iso.3.6.1.4.1.8691.10.1240.10.4.1.4.1 = INTEGER: 9614
iso.3.6.1.4.1.8691.10.1240.10.4.1.4.2 = INTEGER: 29649
iso.3.6.1.4.1.8691.10.1240.10.4.1.4.3 = INTEGER: 0
...
iso.3.6.1.4.1.8691.10.1240.10.4.1.5.1 = INTEGER: 6807
iso.3.6.1.4.1.8691.10.1240.10.4.1.5.2 = INTEGER: 27534
iso.3.6.1.4.1.8691.10.1240.10.4.1.5.3 = INTEGER: 0
...
iso.3.6.1.4.1.8691.10.1240.10.4.1.6.1 = INTEGER: 10010
iso.3.6.1.4.1.8691.10.1240.10.4.1.6.2 = INTEGER: 32732
iso.3.6.1.4.1.8691.10.1240.10.4.1.6.3 = INTEGER: 0
```

Там, где в ModBus возвращается float32, в SNMP используется Integer, насколько я понимаю,
нужно делить на 1000.

Вставил в Templates шаблоны графиков для линий (для каждой линии вывожу минимум, текущее значение, максимум)

### Устройство E1210

Префикс для SNMP — `SNMPv2-SMI::enterprises.8691.10.1210.`, 16 линий, структура по каждой:

```
DIEntry ::= SEQUENCE {
                diIndex     Integer32,
                diType      Integer32,
                diMode      Integer32,
                diStatus    Integer32,
                diFilter    Integer32,
                diTrigger   Integer32,
                diCntStart  Integer32   
            }
```

Пример выдачи `snmpwalk`:

```
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.1.1 = INTEGER: 0
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.1.2 = INTEGER: 1
...
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.1.16 = INTEGER: 15

SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.2.1 = INTEGER: 0
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.2.2 = INTEGER: 0
...
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.2.16 = INTEGER: 0

SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.3.1 = INTEGER: 0
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.3.2 = INTEGER: 0
...
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.3.16 = INTEGER: 0

SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.4.1 = INTEGER: 1
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.4.2 = INTEGER: 0
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.4.3 = INTEGER: 0
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.4.4 = INTEGER: 0
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.4.5 = INTEGER: 1
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.4.6 = INTEGER: 0
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.4.7 = INTEGER: 1
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.4.8 = INTEGER: 0
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.4.9 = INTEGER: 0
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.4.10 = INTEGER: 0
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.4.11 = INTEGER: 0
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.4.12 = INTEGER: 0
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.4.13 = INTEGER: 0
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.4.14 = INTEGER: 0
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.4.15 = INTEGER: 0
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.4.16 = INTEGER: 0

SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.5.1 = INTEGER: 100
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.5.2 = INTEGER: 100
...
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.5.16 = INTEGER: 100

SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.6.1 = INTEGER: 0
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.6.2 = INTEGER: 0
...
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.6.16 = INTEGER: 0
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.7.1 = INTEGER: 0
...
SNMPv2-SMI::enterprises.8691.10.1210.10.1.1.7.16 = INTEGER: 0
```

Наши линии работают в режиме Status (не счётчик), поэтому нас больше всего
интересуют OID-ы, заканчивающиеся на 1.1.4.__номер_линии__ (diStatus). Создал
для них item-ы в шаблоне.


### Устройство E1260

У устройства шесть RTD (Resistive Thermal Data?) датчиков,
нас интересуют OID `iso.3.6.1.4.1.8691.10.1260.10.6.1.5.#`, где '#' показывает номер линии.
