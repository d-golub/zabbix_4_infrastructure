# Работа с Modbus устройствами с помощью библиотеки pymodbus

Поставил через PIP на своей машине библиотеку pymodbus.
Описание библиотеки на сайте [ReadTheDocs](https://pymodbus.readthedocs.io).

```
%> pip search pymodbus
pymodbus (1.3.2)     - A fully featured modbus protocol stack in python
  INSTALLED: 1.3.2 (latest)
pyModbusTCP (0.1.3)  - A simple Modbus/TCP library for Python
```

На данный момент соединения обрываются с диагностикой "Timeout), в командной строке это было, когда вместо
инкапсулированного Modbus-RTU в качестве протокола указывали Modbus-TCP.

## Получение данных в интерактивной сессии iPython

В интерактивной сессии ipython удалось добиться какой-то работы Modbus, для этого сделано вот что:

```{python}

: from pymodbus.client.sync import ModbusTcpClient
: from pymodbus.transaction import ModbusRtuFramer
: client = ModbusTcpClient('10.1.129.140', 4004, framer=ModbusRtuFramer)
: client.connect()
True

In [26]: result = client.read_input_registers(2016, count=1, unit=1)

In [27]: result
Out[27]: <pymodbus.pdu.ExceptionResponse at 0x7fe2064d6c18>

In [28]: print(result)
Exception Response(132, 4, IllegalFunction)
```

Уже что-то. Попробую другую функцию чтения (данный счётчик почему-то не хочет работать с read_input_registers -- функцией 3):

```{python}
In [32]: result = client.read_holding_registers(3000, count=2, unit=1)

In [33]: print(result)
ReadRegisterResponse (2)
```

Считаны какие-то данные. Посмотреть, что внутри, можно с помощью поля (accessor-а?) `registers`.

```
In [56]: result.registers
Out[56]: [11384, 14]
```

## Замечание об адресации

Из сравнения данных, полученных через Python библиотеку pymodbus и утилиту modpoll видно, что адрес для
pymodbus должен быть на единицу меньше, чем для modpoll. Для сравнения запросим модель устройства:

modpoll:
```
%> /opt/ModPoll/bin/modpoll -m enc -p 4004  -a 1 -t 4 -r 50 -c 6 -1 10.1.129.140                           >>>>>>>>>>
...
[50]: 26949
[51]: 19763
[52]: 12597
[53]: 13600
[54]: 8224
[55]: 8224
```

pymodbus:

```{python}
result = client.read_holding_registers(49, count=6, unit=1); print(result.registers)
[26949, 19763, 12597, 13600, 8224, 8224]
```

Обратите внимание на разницу в адресации (49 для pymodbus, 50 для modpoll).

## Разбирательство с объектом result.

Объект принадлежит к классу `pymodbus.register_read_message.ReadHoldingRegistersResponse`.
Насколько я сейчас вижу из документации, нужно пользоваться интерфейсом класса
[`pymodbus.payload.BinaryPayloadDecoder`](https://pymodbus.readthedocs.io/en/latest/library/payload.html#pymodbus.payload.BinaryPayloadDecoder).  У этого объекта есть метод `fromRegisters`, которому можно скормить список регистров и потом разобрать его, как в 
[примере](https://pymodbus.readthedocs.io/en/latest/examples/modbus-payload.html)

Попытаюсь считать строки с названием и моделью устройства и декодировать их. В интерактивной сессии:

```
result = client.read_holding_registers(29, count=40, unit=1); print(result.registers)
[17774, 25970, 26489, 8269, 25972, 25970, 8224, 8224, 8224, 8224, 8224, 8224, 8224, 8224, 8224, 8224, 8224, 8224, 8224, 8224, 26949, 19763, 12597, 13600, 8224, 8224, 8224, 8224, 8224, 8224, 8224, 8224, 8224, 8224, 8224, 8224, 8224, 8224, 8224, 8224]

from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.constants import Endian

decoder = BinaryPayloadDecoder.fromRegisters(result.registers, endian=Endian.Big)
decoded = {"Name": decoder.decode_string(40), "model": decoder.decode_string(40)}
print(decoded)
{'Name': b'Energy Meter                            ', 'model': b'iEM3155                                 '}
```

И хотя это байты, вполне уже можно разобрать, что к чему.

Попробую ещё с силой тока по фазам и средней:

```
result = client.read_holding_registers(2999, count=40, unit=1)
```

(считал блок электрических параметров)

```
decoder = BinaryPayloadDecoder.fromRegisters(result.registers, endian=Endian.Big)
```

```
In [126]: I_1 = decoder.decode_32bit_float()
In [127]: I_2 = decoder.decode_32bit_float()
In [128]: I_3 = decoder.decode_32bit_float()
In [129]: I_mean = decoder.decode_32bit_float()
In [130]: print(I_1, I_2, I_3, I_mean)
23.489906311035156 31.84847068786621 8.160247802734375 nan
```

В последнем значении ерунда, это потому, что средний ток начинается с регистра
3010, нужно пропустить ещё несколько байт. Текущий указатель в блоке данных
можно получить через поле decoder._pointer:

```
In [132]: decoder._pointer
Out[132]: 16

In [133]: decoder.skip_bytes(4)
In [134]: I_mean = decoder.decode_32bit_float()

In [135]: print(I_1, I_2, I_3, I_mean)
23.489906311035156 31.84847068786621 8.160247802734375 21.166208267211914
```

Со смещения 20W (40B) начинается блок напряжений. Декодирую в словарь:

```
In [136]: decoder._pointer
Out[136]: 24

In [137]: decoder.skip_bytes(16)
In [138]: decoder._pointer
Out[138]: 40

In [141]: voltages = {
     ...: "1-2": decoder.decode_32bit_float(),
     ...: "2-3": decoder.decode_32bit_float(),
     ...: "3-1": decoder.decode_32bit_float(),
     ...: 'Mean_L-L': decoder.decode_32bit_float(),
     ...: "1-N": decoder.decode_32bit_float(),
     ...: "2-N": decoder.decode_32bit_float(),
     ...: "3-N": decoder.decode_32bit_float(),
     ...: "Dummy": decoder.decode_32bit_float(),
     ...: "Mean_L-N": decoder.decode_32bit_float()
     ...: }

In [142]: voltages
Out[142]: 
{'1-2': 404.62579345703125,
 '1-N': 233.3365020751953,
 '2-3': 403.1892395019531,
 '2-N': 231.1866455078125,
 '3-1': 405.4216003417969,
 '3-N': 236.01866149902344,
 'Dummy': nan,
 'Mean_L-L': 404.4122314453125,
 'Mean_L-N': 233.51393127441406}
```

Со смещения 54W начинается блок мощностей. Можно отсчитать количество
пропускаемых байтов, а можно сбросить указатель и переместить его на нужный
байт:

```
In [146]: decoder.reset()

In [147]: decoder._pointer
Out[147]: 0

In [148]: decoder.skip_bytes(54*2)
```

Но тут-то ничего и не вышло, потому что я не считал достаточно данных. Повторю
считывание и продолжу с этого же места.

```
In [156]: result = client.read_holding_registers(2999, count=76, unit=1)

In [157]: print(result)
Modbus Error: [Input/Output] timed out
```

Видимо, такой большой блок нуждается в более длинном таймауте.  Но увеличение
таймаута (поле `timeout` объекта `client`) не помогло.  Опытным путём убедился,
что в конкретном случае нашего счётчика, подключенного через Moxa, можно за
один раз считать примерно 50 слов.

```

```


