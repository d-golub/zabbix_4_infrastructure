---
title: Настройка отправки e-mail уведомлений из Zabbix

---

# Проблема

Вроде-бы-настроенная почта в Zabbix не отправляется, диагностика «Failure when receiving data from the peer».

## Тестирование с помощью `swaks`:

Протестировал со своей машины и как-бы-с-Заббикс-сервера (с помощью проброса портов)


```
%> swaks -p ESMTPS --to d.golub@hostco.ru --server localhost -p 1587 --from zabbixadm@hostco.ru --auth login --auth-user zabbixadm@hostco.ru --auth-password ###### --h-Subject "A test e-mail #2 from Zabbix admin user"
=== Trying localhost:1587...
=== Connected to localhost.
<-  220 P-HOST-EX2K16-1.hostco.ru Microsoft ESMTP MAIL Service ready at Fri, 2 Feb 2018 19:32:13 +0500
 -> EHLO gdgbook.hostco.int
<-  250-P-HOST-EX2K16-1.hostco.ru Hello [10.1.96.148]
<-  250-SIZE 37748736
<-  250-PIPELINING
<-  250-DSN
<-  250-ENHANCEDSTATUSCODES
<-  250-STARTTLS
<-  250-AUTH GSSAPI NTLM LOGIN
<-  250-8BITMIME
<-  250-BINARYMIME
<-  250 CHUNKING
 -> AUTH LOGIN
<-  334 VXNlcm5hbWU6
 -> emFiYml4YWRtQGhvc3Rjby5ydQ==
<-  334 UGFzc3dvcmQ6
 -> ########
<-  235 2.7.0 Authentication successful
 -> MAIL FROM:<zabbixadm@hostco.ru>
<-  250 2.1.0 Sender OK
 -> RCPT TO:<d.golub@hostco.ru>
<-  250 2.1.5 Recipient OK
 -> DATA
<-  354 Start mail input; end with <CRLF>.<CRLF>
 -> Date: Fri, 02 Feb 2018 19:32:14 +0500
 -> To: d.golub@hostco.ru
 -> From: zabbixadm@hostco.ru
 -> Subject: A test e-mail #2 from Zabbix admin user
 -> Message-Id: <20180202193214.022126@gdgbook.hostco.int>
 -> X-Mailer: swaks v20170101.0 jetmore.org/john/code/swaks/
 -> 
 -> This is a test mailing
 -> 
 -> .
<-  250 2.6.0 <20180202193214.022126@gdgbook.hostco.int> [InternalId=41077067219020, Hostname=P-HOST-EX2K16-1.hostco.ru] 1447 bytes in 1.146, 1,233 KB/sec Queued mail for delivery
 -> QUIT
<-  221 2.0.0 Service closing transmission channel
=== Connection closed with remote host.

```

Но Заббикс не хочет отправлять письма, ошибка.  Возможно, глюки с DNS, на всякий случай дописал почтовый сервер в `/etc/hosts`.

## Тестирование с помощью `curl`

Из документации на Заббикс 3.4 ясно, что отправка почты производится с помощью библиотеки
libcurl. 

Поставил 'curl', пробую отправить почту из CLI:

```
curl --url 'smtp://mail.hostco.ru:587' --ssl-reqd \
  --mail-from 'zabbixadm@hostco.ru' --mail-rcpt 'd.golub@hostco.ru' \
  --upload-file letter.txt --user 'zabbixadm@hostco.ru:password' --insecure
```

Где в файле `letter.txt` минимальное письмо:

```
appliance@infrazab:/tmp$ cat letter.txt 
From: Zabbix administrator <zabbixadm@hostco.ru>
To: Dmitry Golub <d.golub@hostco.ru>
Subject: A test 1

11111111111
```

При запуске команды получаем интересное (пароль намеренно указан неправильный, до него дело не доходит):

```
appliance@infrazab:/tmp$ curl --verbose --url 'smtp://mail.hostco.ru:587' --ssl-reqd --mail-from 'zabbixadm@hostco.ru' --mail-rcpt d.golub@hostco.ru --upload-file letter.txt --user zabbixadm@hostco.ru:password --insecure
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0*   Trying 10.1.240.22...
* Connected to mail.hostco.ru (10.1.240.22) port 587 (#0)
< 220 P-HOST-EX2K16-1.hostco.ru Microsoft ESMTP MAIL Service ready at Mon, 5 Feb 2018 15:22:33 +0500
> EHLO letter.txt
< 250-P-HOST-EX2K16-1.hostco.ru Hello [10.1.96.148]
< 250-SIZE 37748736
< 250-PIPELINING
< 250-DSN
< 250-ENHANCEDSTATUSCODES
< 250-STARTTLS
< 250-AUTH GSSAPI NTLM LOGIN
< 250-8BITMIME
< 250-BINARYMIME
< 250 CHUNKING
> STARTTLS
< 220 2.0.0 SMTP server ready
* found 148 certificates in /etc/ssl/certs/ca-certificates.crt
* found 592 certificates in /etc/ssl/certs
* ALPN, offering http/1.1
* SSL connection using TLS1.2 / ECDHE_RSA_AES_128_GCM_SHA256
*        server certificate verification SKIPPED
*        server certificate status verification SKIPPED
*        common name: P-HOST-EX2K16-1 (does not match 'mail.hostco.ru')
*        server certificate expiration date OK
*        server certificate activation date OK
*        certificate public key: RSA
*        certificate version: #3
*        subject: CN=P-HOST-EX2K16-1
*        start date: Thu, 29 Dec 2016 06:46:24 GMT
*        expire date: Wed, 29 Dec 2021 06:46:24 GMT
*        issuer: CN=P-HOST-EX2K16-1
*        compression: NULL
* ALPN, server did not agree to a protocol
> EHLO letter.txt
< 250-P-HOST-EX2K16-1.hostco.ru Hello [10.1.96.148]
< 250-SIZE 37748736
< 250-PIPELINING
< 250-DSN
< 250-ENHANCEDSTATUSCODES
< 250-AUTH GSSAPI NTLM LOGIN
< 250-8BITMIME
< 250-BINARYMIME
< 250 CHUNKING
> AUTH GSSAPI
< 334 GSSAPI supported
* gss_init_sec_context() failed: : No Kerberos credentials available
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
* Closing connection 0
curl: (56) Failure when receiving data from the peer
appliance@infrazab:/tmp$ 
```

То есть `curl` пытается использовать GSSAPI с ненастроенным Kerberos на локальной машине.
Способа указать `curl`, что необходимо использовать `LOGIN`, я не нашёл.  Возможно, и Заббикс 
тоже пытается использовать GSSAPI? Попробую сделать дамп пакетов между почтовым сервером и Zabbix,
используя `tcpdump` (тоже пришлось установить на апплайанс).

Ага, дудки, после STARTTLS ничего интересного уже не видно, хоть MITM атаку устраивай.

В логе самого Заббикса не очень информативно:

```
954:20180205:154334.726 failed to send email: Failure when receiving data from the peer
```

Попытаюсь подменить отправщик почты для Заббикс, для этого руководствовался 
[статьёй в Web](https://www.tecmint.com/configure-zabbix-to-send-email-alerts-to-gmail/): 

```
appliance@infrazab:/usr/lib/zabbix/alertscripts$ pwd
/usr/lib/zabbix/alertscripts
appliance@infrazab:/usr/lib/zabbix/alertscripts$ ls -l 
total 4
-rwxr-xr-x 1 root root 50 Feb  5 15:59 notify-mail.sh
appliance@infrazab:/usr/lib/zabbix/alertscripts$ cat notify-mail.sh
#!/bin/bash
echo "Not implemented yet" >&2
exit 1
appliance@infrazab:/usr/lib/zabbix/alertscripts$ 
```

После прочтения документации: метод уведомлений («media») E-Mail не пользуется скриптами, но можно определить собственный
метод уведомления и написать в нём какой надо скрипт.  Так что нужен маленький SMTP клиент,
типа `ssmtp`.

# Решение проблемы

Прямым способом проблему не решить (нужно конфигурировать Exchange или ставить
свой почтовый сервер), придётся пользоваться обходным вариантом. Установил
пакет «[msmtp](http://msmtp.sourceforge.net/doc/msmtp.html)» из стандартного
репозитория, написал пользователю `zabbix` конфигурационный файл
`/etc/msmtp.d/zabbix` (потому что у него нет домашнего каталога):

```
defaults
   host mail.hostco.ru
   port 587
   protocol smtp
   auto_from off
   maildomain hostco.ru
   auth login
   tls on
   tls_starttls on
   tls_certcheck off

account host
   from zabbixadm@hostco.ru
   user zabbixadm@hostco.ru
   password ###########
   domain gdgbook.hostco.ru
   logfile /tmp/msmtp-zabbix.log
```

Проверил из командной строки:

```
appliance@infrazab:/usr/lib/zabbix/alertscripts$ cat /tmp/letter.txt 
From: Zabbix administrator <zabbixadm@hostco.ru>
To: Dmitry Golub <d.golub@hostco.ru>
Subject: A test 7

777777777
$
$ sudo -u zabbix /usr/bin/msmtp -C /etc/msmtp.d/zabbix -a host d.golub@hostco.ru < /tmp/letter.txt 
```

Работает. Теперь нужно сделать custom media в Заббиксе.  Написал простой скрипт `notify-mail.sh` в каталоге 
`/usr/lib/zabbix/alertscripts`:

```
#!/bin/bash
TO="$1"
SUBJ="$2"
BODY="$3"

/usr/bin/msmtp -C /etc/msmtp.d/zabbix -a host "$TO" <<_EOF_
From: Zabbix administrator <zabbixadm@hostco.ru>
To: ${TO}
Subject: ${SUBJ}

${BODY}
_EOF_
```

Создал кастомный метод уведомлений в Заббиксе (скриншот):

![скриншот настройки метода][fignotify]

[fignotify]: img/notify-mail.png "Настройка custom media" {#fig:notify width=90%}

Путь к файлу можно не указывать, он находится в умолчальном каталоге со скриптами,
которые шлют алерты.

Проверил из командной строки:

```
appliance@infrazab:/usr/lib/zabbix/alertscripts$ sudo -u zabbix ./notify-mail.sh d.golub@hostco.ru "Test 6" "6666666"
```

Письмо приходит. Так и будем жить.

После настройки сообщения от триггеров начали наполнять почтовый ящик.

На закладке «Options» созданного метода уведомлений указал, что одновременно может быть запущено 5 процессов
уведомлений, интервал повтора 30 секунд.


