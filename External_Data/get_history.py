#!/usr/bin/env python

"""
Получение исторических данных из Заббикса (прототип).
На входе: YAML файл с описанием, какие данные нужно получить.
На выходе: CSV файл, где колонки = поля, а строки соответствуют различным
  временам снятия измерений.
Для прототипа ограничимся одним хостом, а также настройку сделаем прямо в
  файле.
"""

import pyzabbix.api as zapi
import yaml
import logging
import logging.config
from time import strftime, gmtime

o_config = yaml.load("""
    version: 1
    ZabbixAPI:
        ip: infrazab.hostco.ru
        user: dgolub
        password: DssvmuNsrot
    Host: 10.1.129.142
    Items:
       - Moxa.1240.AI1
       - Moxa.1240.AI2
    """)

o_log_config = yaml.load("""
    version: 1
    formatters:
        simple:
            format: '%(asctime)s: %(name)s - %(levelname)s - %(message)s'
        brief:
            format: '%(name)s:  %(levelname)s - %(message)s'
    handlers:
      console:
        class : logging.StreamHandler
        formatter: brief
        level   : WARNING
        stream  : ext://sys.stderr
      logfile:
        class : logging.handlers.RotatingFileHandler
        formatter: simple
        encoding: utf8
        level: DEBUG
        filename: /tmp/ZabbixExport.log
        # Max log file size: 1 MB, then the file will be rotated
        maxBytes: 1048576
        backupCount: 1
    root:
        level: DEBUG
        handlers: [console, logfile ]
    loggers:
        __main__:
            level: DEBUG
            handlers: [console, logfile]
        """)


def work(o_config, o_log):
    s_url = 'http://' + o_config['ZabbixAPI']['ip'] + '/zabbix'
    o_api = zapi.ZabbixAPI(url=s_url,
                           user=o_config['ZabbixAPI']['user'],
                           password=o_config['ZabbixAPI']['password'])
    # *DBG( print(str(o_api))
    lsHosts = o_api.host.get(filter={'name': o_config['Host']})
    l_history = []
    for dHost in lsHosts:
        d_items = {}
        i_host_id = int(dHost['hostid'])
        # такой хост существует и нам известен его идентификатор
        for s_key in o_config['Items']:
            o_items = o_api.item.get(filter={'hostid': i_host_id, 'key_': s_key})
            # нет причины ловить здесь ошибку, так как если item найден,
            # то и идентификатор у него существует
            if len(o_items) > 0:
                i_itemid = o_items[0]['itemid']
                i_itemtype = o_items[0]['value_type']
                d_items[i_itemid] = {'key': s_key, 'valtyp': i_itemtype}
        o_log.debug("Items found: " + str(d_items))
        # у нас есть идентификаторы хоста и айтема, теперь можно спросить историю.
        for i_item in d_items.keys():
            o_log.debug('Getting history for item {} (key {})'.format(i_item, d_items[i_item]))
            o_history_data = o_api.history.get(hostid=i_host_id, history=0, itemids=str(i_item), limit=20)
            oLog.debug("Item ID: " + str(i_item))
            for o_history_item in o_history_data:
                i_id = o_history_item['itemid']
                d_data = {"TimeStamp": o_history_item['clock'],
                          "itemid": i_id,
                          'item_name': d_items[i_id]['key'],
                          'value': o_history_item['value']}

                d_data['TimeStamp'] = strftime("%Y-%m-%d_%H:%M:%S", gmtime(int(d_data['TimeStamp'])))
                o_log.debug('data found: {}'.format(str(d_data)))
                l_history.append(d_data)
    return l_history


if __name__ == "__main__":
    logging.config.dictConfig(o_log_config)
    oLog = logging.getLogger('Zabbix_Export')
    oLog.info("Program start")
    oLog.debug(o_config)
    print("\n".join([str(l) for l in work(o_config, oLog)]))
    oLog.info("Program end")
