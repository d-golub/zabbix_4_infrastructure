#!/usr/bin/env python3

"""
Тестовая программа для работы со счётчиком iEM 3155 по ModBus и выдаче данных в StdOut в JSON.
"""

# external requirements
from pymodbus.client.sync import ModbusTcpClient
from pymodbus.transaction import ModbusSocketFramer
from pymodbus.payload     import BinaryPayloadDecoder
from pymodbus.constants   import Endian
from sys import stdout
from numbers import Number
import logging
import json

# constants
BIGENDIAN = Endian.Big


# helper functions
def o_decode_fn(o_regs):
    return BinaryPayloadDecoder.fromRegisters(
                o_regs,
                endian=BIGENDIAN)


# set up logging
logging.basicConfig()
log = logging.getLogger()
log.setLevel(logging.INFO)


# === classes ===
class ElectricMeterData:
    def __init__(self):
        self.current = {}
        self.voltage = {}
        self.power = {}
        self.energy = {}

    def json(self):
        """returns a JSON-formatted string with values of electric parameters
        """
        return json.dumps({
                  # filter out empty values (Nones, NaNs etc)
                  "Current": {k: v for k, v in self.current.items() if v and isinstance(v, Number)},
                  "Voltage": {k: v for k, v in self.voltage.items() if v and isinstance(v, Number)},
                  "Power":   {k: v for k, v in self.power.items() if v and isinstance(v, Number)},
                  "Energy":  {k: v for k, v in self.energy.items() if v and isinstance(v, Number)}
                })


# ==== main ===
o_client = ModbusTcpClient('10.1.129.140', 502, framer=ModbusSocketFramer)
o_client.connect()


try:
    o_electricRes = o_client.read_holding_registers(2999, count=38, unit=1)
    o_electric_dec = o_decode_fn(o_electricRes.registers)
    o_powerRes = o_client.read_holding_registers(3053, count=8, unit=1)
    o_power_dec = o_decode_fn(o_powerRes.registers)
    o_energyRes = o_client.read_holding_registers(3203, count=4, unit=1)
    o_energy_dec = o_decode_fn(o_energyRes.registers)
    o_energyByPhaseRec = o_client.read_holding_registers(3518, count=12, unit=1)
    o_energyByPhase_dec = o_decode_fn(o_energyByPhaseRec.registers)
except Exception:
    o_client.close()
    log.fatal("Cannot read data from device")

o_client.close()

# decode data from decoder objects. XXX All constants are nailed!
d_current = {
        "L1": o_electric_dec.decode_32bit_float(),
        "L2": o_electric_dec.decode_32bit_float(),
        "L3": o_electric_dec.decode_32bit_float(),
        "dummy": o_electric_dec.skip_bytes(8),
        "Mean": o_electric_dec.decode_32bit_float()
}

log.debug("Currents from meter:")
log.debug(str(d_current))

# skip to voltage section
o_electric_dec.skip_bytes(16)

d_voltage = {
        # Used '_' instead of '-' due to Zabbix JSON preprocessing limits
        "L1_L2": o_electric_dec.decode_32bit_float(),
        "L2_L3": o_electric_dec.decode_32bit_float(),
        "L3_L1": o_electric_dec.decode_32bit_float(),
        'Mean_L_L': o_electric_dec.decode_32bit_float(),
        "L1_N": o_electric_dec.decode_32bit_float(),
        "L2_N": o_electric_dec.decode_32bit_float(),
        "L3_N": o_electric_dec.decode_32bit_float(),
        "Dummy": o_electric_dec.skip_bytes(4),
        "Mean_L_N": o_electric_dec.decode_32bit_float()
}
log.debug("Voltages from meter:")
log.debug(str(d_voltage))

# decode power values
d_powers = {
        "L1": o_power_dec.decode_32bit_float(),
        "L2": o_power_dec.decode_32bit_float(),
        "L3": o_power_dec.decode_32bit_float(),
        "Full": o_power_dec.decode_32bit_float(),
}
log.debug("powers from meter:")
log.debug(str(d_powers))

# decode energy counters
d_energy = {
        "FullActive": o_energy_dec .decode_64bit_int(),
        "L1": o_energyByPhase_dec.decode_64bit_int(),
        "L2": o_energyByPhase_dec.decode_64bit_int(),
        "L3": o_energyByPhase_dec.decode_64bit_int(),
}
log.debug("Energy from meter:")
log.debug(str(d_energy))

o_meterData = ElectricMeterData()
o_meterData.current = d_current
o_meterData.voltage = d_voltage
o_meterData.power = d_powers
o_meterData.energy = d_energy

log.debug("All data copied to o_meterData structure")
stdout.write(o_meterData.json() + "\n")

# end of program
