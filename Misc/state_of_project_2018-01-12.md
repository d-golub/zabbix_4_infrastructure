---
title: Состояние проекта "Тестирование Заббикс для мониторинга инженерной инфраструктуры Хост" на 12.01.2018
---

# Сделано

## Ноябрь:

  1. Читал, что такое ModBus, как передаются данные и какие они бывают. Добился некоторого понимания.

  2. Развёрнут тестовый Заббикс 3.4 на виртуалке `t-osic-infrazab` (10.1.96.148).

  3. Написал прототип программы сбора данных электросчётчика на Python3, запустил на тестовой машине.

  4. По опыту Протека пришёл к тому, что Python не лучший язык для подобных решений,
     так как появляется миллион зависимостей и возникают проблемы при обновлении.  Выбрал писать
     программу поддержки счётчиков на Rust (www.rust-lang.org).

  5. Придумал общую функциональность программы. Она вызывается Заббиксом как внешний сборщик данных,
     считывает файл с описанием счётчика (что меряется и в каких регистрах ModBus можно это считать),
     выдаёт на стандартный вывод JSON строки со значениями.  Соответственно, в Item (объект данных Заббикса) 
     попадает JSON строка, которая потом средствами Заббикса разбирается на много Item-ов для 
     конкретных параметров.
     Такой подход позволяет вызывать программу один раз и считать, например, два десятка параметров за этот вызов,
     вместо того, чтобы дёргать программу на каждый параметр.

Вот сейчас сообразил, что Полина не видела, что получилось. Открыл доступ пользователю `guest` без пароля
на тестовый Заббикс, выставил ему начальным экраном данные с электросчётчика.

Заходите: http://infrazab.hostco.ru/zabbix/

## Декабрь:

  1. Изучаю Rust. Наткнулся, конечно, на много трудностей: идеологически язык отличается от того, с чем
     мне раньше приходилось иметь дело.

  2. Написал несколько простых тестовых программ для отработки разных кусков большей проблемы: разбора
     аргументов командной строки, запроса данных через ModBus, разбора YAML (описания счётчиков) и вывода JSON.

  3. Пишу программу, текущее состояние есть на
     [bitbucket](https://www.bitbucket.org), но там репозиторий закрытый и
     нужно давать доступ индивидуально.  Наверное, закажу репозиторий в Хосте,
     чтобы всем участникам проекта было видно.

  4. Сейчас занимаюсь обработкой ошибок: у нас в программе много мест, где что-то может пойти не так,
     и нужно обрабатывать эти ситуации (в простейшем случае — завершая программу с кодом возврата и
     диагностикой «всё плохо!».

# Прекрасное будущее

## Краткосрочная перспектива

  1. Работа с ModBus в программе (сейчас в этом месте заглушка, которая всегда выдаёт одинаковые данные),
     для этого у меня есть эмулятор ModBus.  Ну и есть надежда, что настоящий контроллер когда-то приедет.

  2. Корректная отработка ошибок.
 

## Долгосрочная перспектива

  1. Простая статистика в Zabbix. Например, расход электроэнергии за 30 дней (скользящее окно) или с начала месяца.

  2. Тестирование программы как способа сбора данных для Zabbix (пока только на своей машине в командной строке запускаю).

  3. Мнемосхемы в Заббиксе.  Если кто-то сможет это взять на себя, буду рад.

  4. Сбор данных с SNMP датчиков в Zabbix. Автоматический поиск SNMP устройств и их включение в Zabbix (discovery).

  5. Триггеры (события и реакция на них).

  6. Настройка электронной почты.

  7. Кастомные экраны (дашборды).

Настройку e-mail и определение триггеров можно сделать уже сейчас, когда работает прототип программы. Нужен только
список рассылки — или могу на int_host_zabbix@hostco.ru пока всё слать.

Для мнемосхем нужен стенд (и его схема, конечно).

Статистику тоже можно теоретически делать хоть сейчас, просто мои мозги заняты программированием :)

# Сложности и "затыки"

Пока моей квалификации программиста не хватает, приходится учиться на ходу.  Это, конечно, как метод учёбы
неплохо, но на сроках разработки сказывается пагубно.

На данный момент не получается обработка ошибок и, возможно, понадобится переделывать типы данных для разных
электрических параметров.  Серёга Яковлев мне помогает, когда у него находится время.
